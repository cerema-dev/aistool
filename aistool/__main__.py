#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" Entry point module for `aistool`.

    On import it loads all the '**.py**' files found directly under
    the **aistool/tasks/** directory.

    The entry point of `aistool` is `main` function.
"""

from . import app
import importlib
import pathlib
import os

# import 'tasks'
root_dir = pathlib.Path(__file__).parent
for path in map(pathlib.Path, os.listdir(root_dir / 'tasks')):
    if path.suffix == '.py':
        name = path.stem
        if name.startswith('__'):
            continue
        importlib.import_module(f'aistool.tasks.{name}')


def main():
    """ Entry point of `aistool`: calls `aistool.app.run` function. """
    app.run()
