#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.file as file
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_file.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


@pytest.mark.skip(reason="TODO: weird result sequence matching ratio")
def test_file_help(tasks):
    file._register(tasks)
    args = ['file', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_file_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], file.usage()).ratio()
    assert ratio >= 0.8


def test_file_data(tasks, data, tmpdir):
    tmp = tmpdir.mkdir("test_file_data")
    tmpf = pathlib.Path(tmp) / 'test.txt'
    file._register(tasks)
    args = ['-v', '_parse', 'file', str(tmpf)]

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    print('>> test_file_data #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)

    with open(data) as f:
        inp = f.read()
        with open(tmpf) as out:
            assert out.read() == inp
        assert result.output == """\
clichain.cli: INFO: create: _parse
clichain.cli: INFO: create: file
clichain.cli: INFO: creating pipeline...
aistool.tasks.file.0: INFO: starting
aistool.tasks.file.0: INFO: output file: {}
clichain.cli: INFO: processing...
clichain.cli: INFO: ----------------------------------------
""".format(tmpf) + inp + """\
aistool.tasks.file.0: INFO: Done.
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: DONE.
"""
