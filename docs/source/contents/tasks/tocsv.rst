tocsv
======================

usage
----------------------------------------

usage: ::

    $ aistool tocsv --help

.. code-block:: none

    Usage: aistool tocsv [OPTIONS] FIELDS CSVFILE
    
      store AIS fields into a CSV file.
    
      Select specific fields from decoded AIS data and store them into a CSV
      file.
    
      The expected fields are specified by an iterable of field names, for
      examples: ::
    
              # the followings are equivalent:
              '"x", "y"'
              '"x","y"
              '("x", "y")'
              '("x","y")'
              '["x", "y"]'
              '["x","y"]'
    
      A CSV column will be created for each selected field. If a field is not
      present into the AIS data then a default value is assigned.
    
      Several options allow to adjust the default value as well as the CSV
      format, default values for those options will be taken from the
      configuration file.
    
      Note the **'date'** field has to be formatted, an option allow to specify
      the expected output format. If the value is `None` it will not be
      formatted and the `None` value will be used instead.
    
      **NOTE**: assuming timezone is always UTC for **'date'** field.
    
      Input data is expected to be a `dict` containing decoded AIS fields.
    
    Options:
      -t, --datetimeformat TEXT   strftime format for 'date' field.
                                  
                                  To see the
                                  full set of format codes supported
                                  on your
                                  platform, consult the strftime(3)
                                  documentation,
                                  or see:
                                  https://docs.python.org/library/datetime.html
                                  If you want to convert to epoch timestamp then
                                  use '%s'. Then the datetime information will
                                  be
                                  converted into (UTC) epoch timestamp.
                                  
                                  NOTE: in
                                  any other case you have to double the
                                  '%' char,
                                  for example:
                                  '%%d/%%m/%%y-%%H:%%M:%%S'
      -f, --field_delimiter TEXT  csv delimiter
      -q, --quote_char TEXT       csv quote char
      -p, --quote_policy TEXT     csv quote policy: QUOTE_ALL / QUOTE_MINIMAL /
                                  QUOTE_NONNUMERIC / QUOTE_NONE
      -d, --default_value TEXT    default value to use when a field is missing
      --help                      Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.tocsv
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

