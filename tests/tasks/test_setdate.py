#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.setdate as setdate
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast


@pytest.fixture
def data_msg4():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_setdate_msg4.txt'
    assert data.exists()
    return data


@pytest.fixture
def data_nmea():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_setdate_nmea.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_msg4():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_setdate_out_msg4.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_nmea():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_setdate_out_nmea.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


def test_setdate_help(tasks):
    setdate._register(tasks)
    args = ['setdate', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_setdate_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], setdate.usage()).ratio()
    assert ratio >= 0.8


def test_setdate_msg4(tasks, data_msg4, output_msg4):
    setdate._register(tasks)
    args = ['-v', '_parse', 'setdate']

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(output_msg4) as f:
        outputs = f.read()

    with open(data_msg4) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_msg4, mode='w') as f:
    #    f.write(result.output)

    print('>> test_setdate_msg4 #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 


def test_setdate_nmea(tasks, data_nmea, output_nmea):
    setdate._register(tasks)
    args = ['-v', '_parse', 'setdate', '--timestamp']

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(output_nmea) as f:
        outputs = f.read()

    with open(data_nmea) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_nmea, mode='w') as f:
    #    f.write(result.output)

    print('>> test_setdate_nmea #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 
