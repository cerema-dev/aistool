gen_nmea
======================

usage
----------------------------------------

usage: ::

    $ aistool gen_nmea --help

.. code-block:: none

    Usage: aistool gen_nmea [OPTIONS]
    
      outputs original NMEA message from decoded message.
    
      Input messages are expected to be a dict containing all the decoded
      fields.
    
      The input message must contain a 'nmea' field whose value is the original
      NMEA data, if it's a multiline message then 'nmea' is expected to be a
      concatenation of the original NMEA sentences, ex: ::
    
          '!AIVDM,2,1,[...]!AIVDM,2,2,[...]'
    
      in this case each original NMEA line will be output separately in the
      order in which they appear in 'nmea' field.
    
      Nothing is output if the decoded message does not contain a 'nmea' field.
    
      It's possible to add a trailing timestamp field in NMEA message if the
      input message contains a (not None) 'date' field, using the appropriate
      option. The (epoch) timestamp field will be added only if the NMEA message
      does not already contain one.
    
      **NOTE**: assuming timezone is always UTC ('date' field)
    
    Options:
      -t, --settimestamp  use 'date' field to add a trailing timestamp
                          field to
                          the NMEA message, only if the NMEA
                          message does not
                          already contain one.
                          The datetime information contained
                          into the
                          'date' field of the input data will be
                          converted to (UTC) epoch timestamp. Nothing
                          will be done
                          if the input data does not
                          contain a 'date' field or
                          that field is None.
                          The action will be done on each NMEA
                          message
                          if the input data was decoded from multiple
                          NMEA
                          lines.
      --help              Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.gen_nmea
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

