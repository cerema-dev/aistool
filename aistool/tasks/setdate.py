#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" add datetime information to AIS messages.

    Use either information contained in NMEA (timestamp) or estimated
    time with message 4 to add date information to decoded AIS data.

    .. note:: assuming timezone is always UTC

    .. seealso:: `usage`
"""

import logging
import click
import ast
from clichain import pipeline
from aistool.app import config, tasks
import sys
from functools import wraps
from datetime import datetime, timedelta


logger = logging.getLogger(__name__)


def usage():
    return """ add 'date' field to AIS data.

        creates a `datetime` object and add it to AIS dictionary as
        'date' field. Input data is expected to be a `dict` containing
        decoded AIS fields.

        the time will be either estimated using AIS message 4 or
        taken from NMEA timestamp field (last field - epoch format).

        + When using message 4 method an algorithm is used to estimate
          a time for each message, ignoring backward date and huge
          leap in time (tolerance in configuration file).

          Message 4 should be received quite often, containing
          information about the current date and time. Then the
          algorithm will use the second information in other AIS
          messages (if present) to assign a time to the message.

          If a message doesn't have a 'timestamp' field or this field
          contains an invalid value then the last estimated time
          (either from last message 4 or last dated message) will be
          used.

          **NOTE**: obviously this method is highly dependable on the
          quality of input data...

        + When using the NMEA timestamp method then the 'nmea' field is
          expected to contain the original NMEA message, then you should
          use appropriate option in decoding process.

        **NOTE**: assuming timezone is always UTC.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='setdate', help=usage())
    @click.option(
        '--timestamp', '-t',
        help="""use NMEA timestamp field to date messages instead of
                using AIS message 4, if this option is set then the
                'nmea' field is expected to contain the original NMEA
                message (use appropriate option when decoding)""",
        is_flag=True,
    )
    @wraps(cli)
    def _cli(*args, **kw):
        return cli(*args, **kw)

    return _cli


def cli(timestamp):
    """ `click` command, see `usage`.

        .. seealso:: `setdate_msg4`, `setdate_timestamp`
    """
    if timestamp:
        return setdate_timestamp()
    else:
        return setdate_msg4()


_register(tasks)


@pipeline.task
def setdate_msg4(ctrl):
    """ Use message 4 to date messages.

        a 'date' field containing a `datetime.datetime` object will
        be added to every message.

        .. note:: the message 4 will be dated as well

        'date' field will be set to `None` until the first (usable)
        message 4 is obtained, then maintain a reference time which is
        updated every time a new date is obtained.

        The reference is initialized by the time information of the
        first message 4.

        Then, for each message:

        + if it's a message 4:

          if the **minute** information is at least one minute ahead of
          the reference then the time information is used to update the
          reference (if the time leap is not too big, see configuration
          file). This is useful to avoid redundant updates when
          receiving many message 4.

        + if it's not a message 4:

          if the message contains a 'timestamp' field and its value is
          valid (i.e < 60, see AIS specification for details) then the
          value will be used to update the reference (only the second
          inforamtion then).

        the 'date' field will be added to every message (including every
        message 4) and its value will be the last reference (last known
        date).

        .. seealso:: `usage`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting: use message 4 method')
    delta = int(config['setdate']['delta'])
    date_max_leap = int(config['setdate']['date_max_leap'])
    ref = None

    with ctrl as push:
        while True:
            msg = yield

            # -------------------------------------------------------- #
            # process message 4                                        #
            # -------------------------------------------------------- #
            if msg['id'] is 4:
                if ref is None or (msg['minute'] != ref.minute):
                    # useful if there are many messages 4...
                    try:
                        newref = datetime(
                            year=msg['year'],
                            month=msg['month'],
                            day=msg['day'],
                            hour=msg['hour'],
                            minute=msg['minute'],
                            second=msg['second'],
                        )
                        if ref is None:
                            ref = newref
                        elif newref < ref:
                            raise ValueError('backward date')
                        elif (newref - ref).total_seconds() > date_max_leap:
                            raise ValueError('too much delta in dates')
                        else:
                            ds = (newref - ref).total_seconds()
                            if ds > 60:
                                # 1 minute because 'timestamp' in
                                # messages is just second
                                _logger.warning(f'time leap > 1 minute : {ds}')
                            ref = newref
                            _logger.info(f'update minute using msg4: {newref}')
                    except Exception as ex:
                        # see default values in message 4 specification...
                        _logger.debug(f'Msg 4 could not be used ({ex}): {msg}')

                msg['date'] = ref  # date msg 4 as well

            # -------------------------------------------------------- #
            # process other messages                                   #
            # -------------------------------------------------------- #
            else:
                msg['date'] = ref
                if ref is None:
                    # need at least one message 4
                    _logger.debug('cannot date msg because no reference yet: '
                                  f'{msg}')
                    push(msg)
                    continue

                second = msg.get('timestamp')
                if second is None:
                    # no timestamp
                    push(msg)
                    continue

                if second >= 60:
                    # unknown (see AIS specification)
                    _logger.debug(f'message has invalid timestamp: {msg}')
                    push(msg)
                    continue

                # compute time using last ref and second information,
                # use value as new ref as well
                if (second - ref.second) > delta:
                    # case msg second is in minute -1
                    msg['date'] = ref - timedelta(minutes=1)
                elif (ref.second - second) > delta:
                    # case msg second is in minute +1
                    msg['date'] = ref + timedelta(minutes=1)
                ref = ref.replace(second=second)
                msg['date'] = ref

            # -------------------------------------------------------- #
            # send message to next stage                               #
            # -------------------------------------------------------- #
            push(msg)

    _logger.info(f'Done.')


@pipeline.task
def setdate_timestamp(ctrl):
    """ Use NMEA timestamp to date messages.

        a 'date' field will be added to every message, containing a
        `datetime.datetime` object or `None`.

        NMEA can contain an epoch timestamp as last field. This method
        is using this field to set a date to the message if the input
        dictionary contains 'nmea' field, and the NMEA message contains
        a timestamp. Otherwise the 'date' field will be added but its
        value will be `None`.

        .. note:: assuming timezone is always UTC

        .. note:: if 'nmea' is a concatenation of NMEA lines as a
            result of multiline message then we assume the timestamp is
            present in the last line as well.

        .. seealso:: `usage`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting: use NMEA timestamp method')
    fromts = datetime.utcfromtimestamp  # assuming TS is always UTC

    with ctrl as push:
        while True:
            msg = yield

            try:
                nmea = msg['nmea']
                # NOTE: we assume this works as well for a multiline
                #       message (see note above in the docstring).
                # NMEA timestamp is the last field
                msg['date'] = fromts(float(nmea.split(',')[-1]))
            except:
                msg['date'] = None

            push(msg)

    _logger.info(f'Done.')
