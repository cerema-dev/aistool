all: install

install: 
	pip install -r requirements.txt
	pip install .

dev: 
	pip install -r requirements-dev.txt
	pip install -e .

freeze: 
	pip freeze | sort | grep -v 'aistool'

test: 
	cd tests && export AISTOOL_CONF='..' && ( pytest -rXxs -vv --cov-report html --cov-report term-missing --cov aistool )

doc: 
	cd docs && export AISTOOL_CONF='..' && make html

