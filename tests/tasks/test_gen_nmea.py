#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.gen_nmea as gen_nmea
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast
import re
from datetime import datetime


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_gen_nmea.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_no_option():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_gen_nmea_out_no_option.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_set_timestamp():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_gen_nmea_out_set_timestamp.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


@pytest.fixture
def preprocess():
    def _preprocess(tasks):
        # 'date':
        # datetime.datetime(2016, 9, 30, 0, 0, 6) -> replace by (2016, 9, 30, 0, 0, 6)
        pattern =  "('date': )(datetime.datetime\\()([0-9, ]+)\\)"
        pattern = re.compile(pattern)

        @pipeline.task
        def pre_process(ctrl):
            parse = ast.literal_eval
            with ctrl as push:
                while True:
                    line = yield
                    line = pattern.sub("\\1(\\3)", line)  # 'date' field
                    try:
                        data = parse(line)
                    except SyntaxError:  # empty line or comment
                        continue
                    try:
                        date = data['date']
                        data['date'] = datetime(*date) if date else None
                    except KeyError:
                        pass
                    push(data)

        @tasks
        @click.command(name='_parse')
        def _pre_process():
            return pre_process()

    return _preprocess


#@pytest.mark.skip(reason="TODO: weird result sequence matching ratio")
def test_gen_nmea_help(tasks):
    gen_nmea._register(tasks)
    args = ['gen_nmea', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_gen_nmea_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], gen_nmea.usage()).ratio()
    assert ratio >= 0.8


def test_gen_nmea_no_option(tasks, data, output_no_option, preprocess):
    gen_nmea._register(tasks)
    preprocess(tasks)  # create _parse
    args = ['-v', '_parse', 'gen_nmea']

    with open(output_no_option) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_no_option, mode='w') as f:
    #    f.write(result.output)

    print('>> test_gen_nmea_no_option #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 


def test_gen_nmea_set_timestamp(tasks, data, output_set_timestamp, preprocess):
    gen_nmea._register(tasks)
    preprocess(tasks)  # create _parse
    args = ['-v', '_parse', 'gen_nmea', '--settimestamp']

    with open(output_set_timestamp) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_set_timestamp, mode='w') as f:
    #    f.write(result.output)

    print('>> test_gen_nmea_set_timestamp #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 
