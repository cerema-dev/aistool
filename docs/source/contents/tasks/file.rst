file
======================

usage
----------------------------------------

usage: ::

    $ aistool file --help

.. code-block:: none

    Usage: aistool file [OPTIONS] FILE
    
      Writes data into the specified file as text.
    
      The string representation of each processed item is written on a single
      line into the specified file.
    
      **NOTE**: sends input data (without alteration) to the next stage of the
      pipeline if any.
    
    Options:
      --help  Show this message and exit.

    
source documentation
----------------------------------------
.. automodule:: aistool.tasks.file
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

