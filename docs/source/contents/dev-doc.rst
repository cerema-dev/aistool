developers documentation
==========================

here's a practical documentation, for source documentation please
see :doc:`aistool`

.. seealso:: `aistool` is based on `clichain`

.. seealso:: `clichain quickstart`_

.. _project_structure:

project structure
----------------------------------------

::

    aistool/
    │
    ├── .gitignore
    ├── LICENSE
    ├── makefile
    [...]
    ├── requirements.txt
    ├── requirements-dev.txt
    |
    ├── aistool/
    │   ├── __init__.py
    │   ├── app.py
    │   |── __main__.py
    │   └── tasks/
    │       ├── __init__.py
    │       ├── <XXX>.py
    │       └── <YYY>.py
    │
    ├── tests/
    │   ├── test_app.py
    │   ├── test_main.py
    │   ├── test_pep8.py
    │   ├── test_tasks.py
    │   └── tasks/
    │       ├── test_<XXX>.py
    │       └── test_<YYY>.py
    │
    └── docs/
        [...]
        └── source/
            ├── conf.py
            ├── index.rst
            └── contents/
                ├── aistool.rst
                ├── dev-doc.rst
                ├── doc.rst
                ├── tasks.rst
                └── tasks/
                    ├── <XXX>.rst
                    └── <YYY>.rst


+ **dependencies**:
  
  ::

    [...]
    ├── requirements.txt
    ├── requirements-dev.txt

  The **requirements.txt** file contains production dependencies. It's
  used by **make install**.
  
  .. note:: for production install, the package can be installed using
      **setup.py** as well, but using **requirements** usually ensures
      any specific version required for a given dependency will be
      used.
  
  The **requirements-dev.txt** file contains development dependencies.
  It includes **requirements** and is used by **make dev**.


+ **sources**:

  .. seealso:: :doc:`aistool`

  Task types implementations are expected to be in the **tasks**
  subdirectory. Every '**.py**' file directly under **tasks** will be
  loaded when the command line tool is run.

  ::

    ├── aistool/
    │   ├── __init__.py
    │   ├── app.py              # common module used to implement tasks
    │   |── __main__.py         # entry point
    │   └── tasks/
    │       ├── __init__.py
    │       ├── <XXX>.py        # implements task "<XXX>"
    │       └── <YYY>.py        # implements task "<YYY>"

+ **tests**:

  Automated tests are run using **make test**. The tests are implemented
  using `pytest`, and `coverage` is used to keep track of the tested
  code. 

  In order to keep consistency with sources, task types implementations
  test code should be put in specific scripts in **tasks** subdirectory.

  ::

    ├── tests/
    │   ├── test_app.py
    │   ├── test_main.py
    │   ├── test_pep8.py        # test PEP8 compliance for everything
    │   ├── test_tasks.py
    │   └── tasks/
    │       ├── data/           # data files used for tests
    │       ├── test_<XXX>.py   # test task "<XXX>" code
    │       └── test_<YYY>.py   # test task "<YYY>" code

+ **docs**:

  The documentation is generated using `sphinx`_, html doc can be
  generated using **make doc**.

  ::

    └── docs/
        [...]
        └── source/
            ├── conf.py
            ├── index.rst           # main index
            └── contents/
                ├── aistool.rst     # common modules source doc
                ├── dev-doc.rst     # developers documentation
                ├── doc.rst         # users documentation
                ├── tasks.rst       # main index for tasks documentation
                └── tasks/
                    ├── <XXX>.rst   # "<XXX>" 's documentation
                    └── <YYY>.rst   # "<YYY>" 's documentation

implementing a task type
----------------------------------------

tasks are implemented using the `clichain` library: ::

    from clichain import pipeline

.. seealso:: `clichain quickstart`_

You also need `click` library to create a `click.command` object: ::

    import click

.. seealso:: `click` documentation

Then you need to register tasks into `aistool`. You'll find two modules
under the `aistool` namespace: ::

    from aistool.app import config, tasks 

+ `aistool.config`:

  this module allows to get values from the configuration file.

  .. seealso:: :ref:`configuration_file`

+ `aistool.tasks` 

  this module allows to register tasks into the interface. `tasks` will
  be used to wrapp and register the created `click.command` object.

Example: ::

    import click
    from clichain import pipeline
    from aistool.app import config, tasks

    import logging
    import ast


    default_group = config['to_object']['default_group_lines'] 


    @pipeline.task
    def create_python_obj(ctrl, group_lines):
        # see "clichain" documentation for details:
        # ========================================
        # process function is a coroutine function, the next input data
        # is then obtained using "yield" keyword. The general pattern
        # is to use "ctrl" as a context manager and use the created
        # "push" function to send data to the next stage of the pipeline
        parse = ast.literal_eval
        group = []
        with ctrl as push:
            while True:
                for _ in range(group_lines):
                    line = yield
                    data = parse(line)  # string representation to python object
                    group.append(data)
                push(group)
                group.clear()

    @tasks
    @click.command(name='to_object')
    @click.option('--group_lines', '-g', help="group lines",
                  default=default_group)
    def parse(group_lines):
        """ create python objects from string representation using ast """
        return create_python_obj(group_lines=int(group_lines))

.. warning:: please respect Python's `PEP8`_. The code will be
    automatically tested against PEP8 when running tests.

We add the expected config value in the configuration file: ::

    [to_object]
    default_group_lines = 2


Using the task:

.. note:: the docstring of the 'parse' function used to create the
    `click.command` object is used here as the help of the command here,
    you can specify a 'help' parameter in the command wrapper... ::

    $ aistool --help
    [...]
    to_object     create python objects from string representation using ast
    [...] 

::

    $ echo '1, 2
    1 + 2
    "abcd"
    {"x": 1, "y": 2}' | aistool to_object
    [(1, 2), 3]
    ['abcd', {'x': 1, 'y': 2}]


logging
----------------------------------------

If we take the previous example we can add logging using the **name**
attribute of the **ctrl** object:

.. seealso:: `logging`, `clichain quickstart`_

::

    @pipeline.task
    def create_python_obj(ctrl, group_lines):
        # see "clichain" documentation for details:
        # ========================================
        # process function is a coroutine function, the next input data
        # is then obtained using "yield" keyword. The general pattern
        # is to use "ctrl" as a context manager and use the created
        # "push" function to send data to the next stage of the pipeline

        logger = logging.getLogger(ctrl.name)
        logger.info('starting')

        parse = ast.literal_eval
        group = []
        with ctrl as push:
            while True:
                for _ in range(group_lines):
                    line = yield
                    data = parse(line)  # string representation to python object
                    logger.debug(f'parsed: {data}')
                    group.append(data)
                push(group)
                group.clear()

        logger.info('ending')

Then the user can use the *verbose* option in main command to adjust the
logging level: ::

    $ aistool --help
    [...]
    -v, --verbose       set the log level: None=WARNING, -v=INFO, -vv=DEBUG
    [...]

The **ctrl.name** attribute can be set by the user in the command line:

::

    $ # see aistool --help for more details
    $ cat data | aistool -vv { 'task1' to_object }
    [...]
    INFO:task1:starting
    DEBUG:task1:parsed: (1, 2)
    DEBUG:task1:parsed: (3, 4)
    INFO:task1:ending
    [...]


testing
----------------------------------------

.. seealso:: :ref:`project_structure`

Considering the task is named "**<name>**":

The test file for the task should be found for example in:
**tests/tasks/test_<name>.py**.

The tests are run using `pytest` library.

.. note:: there's a directory containing data files in the test
    hierarchy, which is used to store data samples used to perform
    the tests. This directory is:

    **tests/tasks/data/**

    Please use this directory to store test data.

Since the tool is an interactive CLI tool, you have to use a specific
interface to be able to test the task within `aistool`.

This interface emulates the execution from the shell.

.. seealso:: `aistool.app.test` for details

example: ::

    import pytest
    import sys
    import aistool.app as app
    import aistool.tasks.<name> as <name>


    @pytest.fixture                                                                 
    def tasks():                                                                    
        from clichain import cli                                                    
        tasks = cli.Tasks()                                                         
        _tasks = app.tasks                                                          
        app.tasks = tasks                                                           
        yield tasks                                                                 
        app.tasks = _tasks


    def test_file_help(tasks):
        args = ['<name>', '--help']
        result = app.test(args, '', catch_exceptions=False)
        print('>> test_<name>_help #1:\n', result.output, file=sys.stderr)
        assert result == "expected..."


.. warning:: please make sure the code coverage is as high as possible
    before committing (usually 100%). There should be a good reason if
    any of the code has not been tested.

.. seealso:: `pytest`

documenting
----------------------------------------

*The documentation is generated using `sphinx`_*

.. seealso:: :ref:`project_structure`

Considering the task is named "**<name>**":

The documentation for the task should be found for example in
**docs/source/contents/tasks/<name>.rst**.

A simple way to document the task is to use `sphinx autodoc`_ which will
generate documentation from the source code.

For example our **<name>.rst** file content could be: ::

    <name>
    ======================

    usage
    ----------------------------------------
    
    usage: ::
    
        $ aistool <name> --help
    
    .. code-block:: none
    
        [ paste the content of "aistool <name> --help" here... ]


    source documentation
    ----------------------------------------
    .. automodule:: aistool.tasks.<name>
        :ignore-module-all:
        :members:


.. note:: documentation files are expected to be in `ReStructuredText`_
    format, and all the files are expected to have a '.rst' suffix.

    .. seealso:: `ReStructuredText documentation`_


.. _clichain quickstart: https://clichain.readthedocs.io/en/latest/contents/doc.html#quickstart
.. _sphinx: http://www.sphinx-doc.org/en/master/
.. _sphinx autodoc: http://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
.. _PEP8: https://www.python.org/dev/peps/pep-0008/ 
.. _ReStructuredText: https://en.wikipedia.org/wiki/ReStructuredText
.. _ReStructuredText documentation: http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html 
