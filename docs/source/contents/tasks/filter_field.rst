filter_field
======================

usage
----------------------------------------

usage: ::

    $ aistool filter_field --help

.. code-block:: none

    Usage: aistool filter_field [OPTIONS] FIELD VALUES
    
      filter messages based on a specific field.
    
      decoded messages are filtered by comparing a specific field to one or
      several accepted values.
    
      input messages are expected to be a dict containing all the decoded
      fields.
    
      **FIELD** specifies which field to compare in the message data.
    
      **VALUES** specifies the accepted values for the specified field. It can
      be either a single value or a sequence of values, example: ::
    
              # single value:
              '319018000'
              '"FAD2407"'
              'True'
    
              # the followings are equivalent:
              '227393830, 319018000, 992476133, 992479633'
              '[227393830, 319018000, 992476133, 992479633]'
              '(227393830, 319018000, 992476133, 992479633)'
              '(227393830,319018000,992476133,992479633)'
    
      **NOTE**: accepted value(s) must be basic types such as integer or
      strings, True/False are also accepted.
    
    Options:
      --help  Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.filter_field
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

