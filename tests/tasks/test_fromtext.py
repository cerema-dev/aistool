#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.fromtext as fromtext
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast
import re
from datetime import datetime


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_fromtext.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_no_option():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_fromtext_out_no_option.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_set_date():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_fromtext_out_date.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_set_date_silent():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_fromtext_out_date_silent.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


@pytest.fixture
def postprocess():
    def _postprocess(tasks):
        @pipeline.task
        def post_process(ctrl):
            with ctrl as push:
                while True:
                    msg = yield
                    assert isinstance(msg, dict)
                    push(msg)

        @tasks
        @click.command(name='_ensure_dict')
        def _post_process():
            return post_process()

    return _postprocess


#@pytest.mark.skip(reason="TODO: weird result sequence matching ratio")
def test_fromtext_help(tasks):
    fromtext._register(tasks)
    args = ['fromtext', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_fromtext_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], fromtext.usage()).ratio()
    assert ratio >= 0.8


def test_fromtext_no_option(tasks, data, output_no_option, postprocess):
    fromtext._register(tasks)
    postprocess(tasks)  # create _ensure_dict
    args = ['-v', 'fromtext', '_ensure_dict']

    with open(output_no_option) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_no_option, mode='w') as f:
    #    f.write(result.output)

    print('>> test_fromtext_no_option #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 


def test_fromtext_set_date(tasks, data, output_set_date, postprocess):
    fromtext._register(tasks)
    postprocess(tasks)  # create _ensure_dict
    args = ['-v', 'fromtext', '--date', '_ensure_dict']

    with open(output_set_date) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_set_date, mode='w') as f:
    #    f.write(result.output)

    print('>> test_fromtext_set_date #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 


def test_fromtext_set_date_silent(tasks, data, output_set_date_silent,
                                  postprocess):
    fromtext._register(tasks)
    postprocess(tasks)  # create _ensure_dict
    args = ['-v', 'fromtext', '--date', '--silent', '_ensure_dict']

    with open(output_set_date_silent) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    print('>> test_fromtext_set_date_silent #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 
