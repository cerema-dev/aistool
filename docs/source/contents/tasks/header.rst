header
======================

usage
----------------------------------------

usage: ::

    $ aistool header --help

.. code-block:: none

    Usage: aistool header [OPTIONS] TIMESTAMP_FORMAT_TYPE
    
      cut the header, extract timestamp and append to NMEA
    
      This task uses 'header' section in configuration: it should contain a
      dictionnary for each header format.
    
      You have to specify one header format to use this task. The header format
      is expected to contain following fields:
    
      NOTE: for more details on regex, see:
      https://docs.python.org/3/howto/regex.html (use "Groups" indicated with
      '(', ')' to capture specific part of the text)
    
      + 'split_header': this regex must **match** the whole header
    
      + 'capture_timestamp': this regex must **capture** the timestamp
    
      + 'timestamp_format': timestamp strftime format
    
        NOTE: you have to double the '%' char, for example:
        '%%d/%%m/%%y-%%H:%%M:%%S'
    
        (to see the full set of format codes supported on your   platform,
        consult the strftime(3) documentation, or see
        https://docs.python.org/3/library/datetime.html)
    
      + 'ignore': (optional) match pattern to ignore specific lines
    
      + 'capture_multiline': this is used only by 'mares_dsi' format,   it
      specifies how to extract information to process multiline   messages: this
      regex must **capture**:
    
          1. the number of line (1,N)
          2. the total number of lines (2,N)
          3. the line ID
    
      The currently defined format are:
    
      ['mares_dsi', 'northsea_dsi', 'satais_alg', 'antilles_guyane']
    
      For each line:
    
      + skip line if empty or match the "ignore" regex
    
      + extract NMEA part using the "split_header" regex
    
        ( log an error and skip the line if any exception )
    
      + extract and parse timestamp part using the "capture_timestamp"   and
      "timestamp_format" regex.
    
        ( silently ignore the header and keep NMEA part only if any   exception
        )
    
      + send "NMEA,timestamp" or "NMEA" to next stage
    
      .. note:: the timestamp will be **UTC** if the original     timestamp is
      not in epoch format (i.e it has to be converted)
    
    Options:
      --help  Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.header
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

