#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" decode raw AIS messages from NMEA format.

    .. seealso:: `usage`
"""

import lpais.ais as ais
import logging
import click
import ast
from clichain import pipeline
from aistool.app import config, tasks
import sys
from functools import wraps


logger = logging.getLogger(__name__)
default_ais_types = str(config.get('decode', 'default_ais_types',
                                   fallback=tuple(range(1, 26))))


def usage():
    return """ decode AIS messages using `lpais.ais`

        AIS messages are decoded from NMEA messages, the line must
        start with '!', i.e any header must be removed before decoding.

        The input line should look like the following example: ::

        \b
        !AIVDM,1,1,,A,14h?D4002:PqoCpGCFVc0Hmh0<1H,0*0E

        If a line doesn't start with '!' it will be silently ignored.

        If the line cannot be decoded (an exception occurs) then the
        exception is logged and the line is ignored.

        If the line is correctly decoded then this produces a dictionary
        containing all the decoded fields.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='decode', help=usage())
    @click.option(
        '--aistypes', '-t',
        help=f"""list of ais message types to keep: examples:

                 "1,2,3..."
                 "1, 2, 3..."
                 "(1,2,3...)"
                 "[1,2,3...]"
                 "1"
                 "1,"

                 default value:
                 {default_ais_types}
              """,
        default=default_ais_types,
    )
    @click.option(
        '--keepnmea', '-k',
        help="""keep the original "nmea" message(s) in the data
                (it will be accessible "nmea" field in the dictionnary,
                 which will be a concatenation of original NMEA lines
                 in case of a multiline message)
             """,
        is_flag=True,
    )
    @wraps(cli)
    def _cli(*args, **kw):
        return cli(*args, **kw)

    return _cli


def cli(aistypes, keepnmea):
    """ `click` command, see `usage` """
    try:
        aistypes = tuple(map(int, ast.literal_eval(aistypes)))
    except:
        raise click.BadParameter(f"incorrect value for aistypes: {aistypes}")

    return decode(aistypes, keepnmea)


_register(tasks)


@pipeline.task
def decode(ctrl, aistypes, keepnmea):
    """ 'decode' coroutine implementation.

        `ctrl` is the control object given to the coroutine function
        (see `clichain.pipeline.task` for details).

        `aistypes` is an iterable containing AIS message types (int)
        to keep, messages are still decoded but will not be sent to
        the next stage of the pipeline.

        `keepnmea` indicates wether or not to keep NMEA line as a 'nmea'
        field in output dictionary. If `True` then the value will be a
        concatenation of original NMEA lines for multiline messages.

        .. seealso:: `usage`, NMEA messages are decoded with `lpais.ais`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    _logger.info(f'ais types to decode: {aistypes}')
    _logger.info(f'keep nmea: {keepnmea}')

    decode = ais.decoder(
        keep_nmea=keepnmea,
        handle_err=_logger.error,
        allow_unknown=True,
        allow_missing_timestamps=True
    )

    with ctrl as push:
        while True:
            line = yield

            # pre-process
            line = line.strip()
            if not line.startswith('!'):
                continue

            # process NMEA
            data = decode(line)
            if data and data['id'] in aistypes:
                push(data)

    _logger.info(f'Done.')
