aistool
=======  

ais data stream pipeline processor

install and test
=======================

.. warning:: user is encouraged to use `gcc`_ to compile dependencies
    (*gcc* and *g++*), otherwise it's likely to fail. Make sure **gcc**
    will be used as default compliler or specify it when install / build

    example:

    .. code-block:: bash

        $ CC=gcc-8 CXX=g++-8 make install


production install
******************

There is a makefile in the project root directory:
    
.. code-block:: bash

    $ make install

Using pip, the above is equivalent to:

.. code-block:: bash

    $ pip install -r requirements.txt                                             
    $ pip install -e .

dev install
****************

There is a makefile in the project root directory:
    
.. code-block:: bash

    $ make dev

.. note:: on **OSX** you should install `gcc`_ with `homebrew`_ and then
    ensure it's used instead of default *gcc* (actually *clang*)

    example:

    .. code-block:: bash

        $ gcc --version
        Apple LLVM version 9.0.0 (clang-900.0.39.2)
        $ brew install gcc
        [...]
        $ gcc-8 --version
        gcc-8 (Homebrew GCC 8.2.0) 8.2.0
        $ CC=gcc-8 CXX=g++-8 make dev

Using pip, the above is equivalent to:

.. code-block:: bash

    $ pip install -r requirements-dev.txt                                             
    $ pip install -e .

.. note:: before contribututing please read the developers documentation

run the tests
******************

Use the makefile in the project root directory:

.. code-block:: bash

    $ make test

This runs the tests generating a coverage html report

.. note:: you can see the last coverage report for master branch `here <https://cerema-dev.frama.io/aistool/htmlcov>`_

build the doc
******************

The documentation is made with sphinx, you can use the makefile in the
project root directory to build html doc:

.. code-block:: bash

    $ make doc

Documentation
=======================

The documentation is hosted on `gitlab pages <https://cerema-dev.frama.io/aistool/html>`_

Meta
=======================

loicpw - peronloic.us@gmail.com

Distributed under the MIT license. See ``LICENSE.txt`` for more information.

https://framagit.org/cerema-dev 

created by loicpw - peronloic.us@gmail.com                                      

https://framagit.org/loicpw


.. _homebrew: https://brew.sh/
.. _gcc: https://gcc.gnu.org/
