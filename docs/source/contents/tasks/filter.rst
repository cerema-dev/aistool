filter
======================

usage
----------------------------------------

usage: ::

    $ aistool filter --help

.. code-block:: none
    
    Usage: aistool filter [OPTIONS] POINTS
    
      filter AIS position messages based on position.
    
      Input messages are expected to be a dict containing all the decoded
      fields.
    
      Filter AIS messages with position information ('x' and 'y' fields),
      according to a specified *zone* which is defined by a polygon (a list of
      points).
    
      The expected polygon is defined by an iterable of *points*, each *point*
      is defined itself by a couple **lon, lat** corresponding to **longitude**
      and **latitude** information (i.e 'x' and 'y').
    
      .. note:: the polygon is automatically closed (the last point     and the
      first point are joined)
    
      example: ::
    
              # the followings are equivalent:
              '(1, 2), (4, 5), (7, 8)'
              '(1,2), (4,5), (7,8)'
              '(1,2),(4,5),(7,8)'
              '((1, 2), (4, 5), (7, 8))'
              '[(1, 2), (4, 5), (7, 8)]'
              '[[1, 2], [4, 5], [7, 8]]'
    
      For every message containing 'x' and 'y' fields, test if the corresponding
      position is included in the expected polygon, if it's included then the
      message is sent to next stage of the pipeline, otherwise it's ignored.
    
      .. note:: will not filter messages not containing 'x' and 'y'     fields,
      those messages will be sent to next stage of the     pipeline.
    
    Options:
      --help  Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.filter
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

