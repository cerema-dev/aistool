#! /usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools

setuptools.setup(
    name="aistool",
    version="0.0.1",
    url="https://framagit.org/cerema-dev/aistool",
    download_url = "https://framagit.org/cerema-dev/aistool.git",

    author="Loïc Peron",
    author_email="peronloic.us@gmail.com",

    description="ais data stream pipeline processor",
    long_description='\n\n'.join(
        open(f, 'rb').read().decode('utf-8')
        for f in ['README.rst', 'HISTORY.rst']),

    packages=setuptools.find_packages(),

    install_requires=[
        'clichain',
        'lpais',
        'pytz',
        'matplotlib',
    ],

    license='MIT License',
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],

    entry_points='''
        [console_scripts]
        aistool=aistool.__main__:main
    ''',
)
