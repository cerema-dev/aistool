setdate
======================

usage
----------------------------------------

usage: ::

    $ aistool setdate --help

.. code-block:: none

    Usage: aistool setdate [OPTIONS]
    
      add 'date' field to AIS data.
    
      creates a `datetime` object and add it to AIS dictionary as 'date' field.
      Input data is expected to be a `dict` containing decoded AIS fields.
    
      the time will be either estimated using AIS message 4 or taken from NMEA
      timestamp field (last field - epoch format).
    
      + When using message 4 method an algorithm is used to estimate   a time
      for each message, ignoring backward date and huge   leap in time
      (tolerance in configuration file).
    
        Message 4 should be received quite often, containing   information about
        the current date and time. Then the   algorithm will use the second
        information in other AIS   messages (if present) to assign a time to the
        message.
    
        If a message doesn't have a 'timestamp' field or this field   contains
        an invalid value then the last estimated time   (either from last
        message 4 or last dated message) will be   used.
    
        **NOTE**: obviously this method is highly dependable on the   quality of
        input data...
    
      + When using the NMEA timestamp method then the 'nmea' field is   expected
      to contain the original NMEA message, then you should   use appropriate
      option in decoding process.
    
      **NOTE**: assuming timezone is always UTC.
    
    Options:
      -t, --timestamp  use NMEA timestamp field to date messages instead of
                       using
                       AIS message 4, if this option is set then the
                       'nmea' field
                       is expected to contain the original NMEA
                       message (use
                       appropriate option when decoding)
      --help           Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.setdate
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

