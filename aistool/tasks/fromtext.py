#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" recreates AIS fields (decoded message) from string representation.

    This is usefull when AIS data has been decoded then printed as it is
    (to a file for instance), and you want to reuse that data.

    the input is expected to be for each line the string representation
    of the `dict` containing decoded AIS fields. It can also include the
    "date" field (see *setdate* task), which is expected to be the
    representation of a `datetime.datetime` object (which will be for
    example: *datetime.datetime(2017, 1, 8, 22, 0)*).

    .. seealso:: `usage`

    .. note:: assuming timezone is always UTC ('date' field)
"""

import logging
import click
from clichain import pipeline
from aistool.app import config, tasks
from functools import wraps
import ast
import re
from datetime import datetime


logger = logging.getLogger(__name__)


def usage():
    return """ recreates AIS fields from string representation.

    usefull to reuse decoded AIS messages previously printed with no
    formatting (i.e the string representation of the dictionnary)

    each input message is expected to be the string representation
    of the dictionnary containing decoded AIS fields. It can also
    include the "date" field as it's added by *setdate* task.

    By default the "date" field will no be parsed and left as a string
    representation of the original value. This will provide better
    performance if no work is performed with this field. A specific flag
    can be set to turn on re-creation of the "date" field value.

    **NOTE**: If the "date" field is present then it's assumed to
    represent a UTC value.

    By default an error log will be emitted every time an input line
    cannot be parsed, this can be inhibited if the *silent* flag is set.

    Empty lines and lines beginning with "#" will not be processed.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='fromtext', help=usage())
    @click.option('--date', '-d',
                  help="""parse "date" field if exists,
                          default will not attempt to parse it and keep
                          its representation as the "date" field value.
                          (this has no effect when no "date" field is
                          present in the message).""",
                  is_flag=True)
    @click.option('--silent', '-s',
                  help="""prevent logging when an input cannot be
                          parsed, by default an error level log will be
                          emitted.""",
                  is_flag=True)
    @wraps(cli)
    def _cli(date, silent):
        return cli(date, silent)

    return _cli


def cli(date, silent):
    """ `click` command, see `usage` """
    return parse(date, silent)


_register(tasks)


@pipeline.task
def parse(ctrl, date, silent):
    """ recreates AIS `dict` from string representation.

        Uses `ast` to recreate the `dict` containing all decoded fields
        from its string representation. All values are parsed by `ast`
        except the **"date"** field if present.

        By default (if `date` is `False`) the **"date"** field value is
        kept as a string value (its representation), i.e as it is in the
        input data (example: *datetime.datetime(2017, 1, 8, 22, 0)*).

        .. note:: leaving `date` as `False` will save compute time if
            the **"date"** field is not used.

        If `date` is `True` then use `datetime.datetime` to recreate
        the datetime object from its string representation where a
        **"date"** field is present.

        .. note:: empty input lines and lines beginning with "#" will be
            silently ignored.

        If `silent` is `True` then no message will be logged when an
        error occurs during parsing. Otherwise a `logging.ERROR` level
        message will be emitted every time an input message could not
        be parsed. In any case the process will continue, ignoring the
        failed message.

        .. seealso:: `usage`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    _parse = ast.literal_eval
    verbose = not silent

    error = _logger.error
    if silent:
        _logger.info('silent mode: no error log when message cannot be parsed')

        def error(*args, **kw):
            pass

    if date:
        _logger.info('will parse "date" field if present')
        datetimeformat = '%Y, %m, %d, %H, %M, %S'
        parsedate = datetime.strptime

    # format used for str(datetime object) is:
    # "datetime.datetime(2016, 9, 30, 0, 0, 6)"
    # this field is modified using re pattern:
    # if date is True this is turned into
    # "'2016, 9, 30, 0, 0, 6'"
    # otherwise it's just wrapped into '' so ast processes it as a string
    # In both cases the same pattern is used
    date_pattern = "('date': )(datetime.datetime\\()([0-9, ]+)\\)"
    date_pattern = re.compile(date_pattern)

    with ctrl as push:
        while True:
            line = yield
            line = line.strip()  # remove extra spaces and eol

            # skip line if empty or comment (begin with '#')
            if not line or line.startswith('#'):
                continue

            # preprocess 'date' field for ast if any
            line = (date_pattern.sub("\\1'\\3'", line) if date else
                    date_pattern.sub("\\1'\\2(\\3)'", line))

            # parse line to dict
            try:
                msg = _parse(line)
                if not isinstance(msg, dict):
                    error(f"not a dict ({type(msg)}): {msg}, line: '{line}'")
                    continue
            except (SyntaxError, ValueError):
                error(f"could not parse line to dict: '{line}'")
                continue

            # parse date if 'date' field and required
            if date:
                try:
                    msg['date'] = parsedate(msg['date'], datetimeformat)
                except KeyError:
                    pass
                except Exception as e:
                    error(f"could not parse 'date' ({e}): {msg}")
                    continue

            # send to next stage
            push(msg)

    _logger.info('Done.')
