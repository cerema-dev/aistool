#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.filter_field as filter_field
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_filter_field.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_MMSI():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_filter_field_out_MMSI.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_callsign():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_filter_field_out_callsign.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_raim():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_filter_field_out_raim.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


@pytest.mark.skip(reason="TODO: weird result sequence matching ratio")
def test_filter_field_help(tasks):
    filter_field._register(tasks)
    args = ['filter_field', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_filter_field_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], filter_field.usage()).ratio()
    # TODO
    assert ratio >= 0.8


def test_filter_invalid_values(tasks):
    filter_field._register(tasks)
    args = ['filter_field', 'x', 'foo']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_filter_invalid_values #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: incorrect values specification: foo" in result.output


def test_filter_field_MMSI(tasks, data, output_MMSI):
    filter_field._register(tasks)
    args = ['-v', '_parse', 'filter_field', 'mmsi', '227393830, 319018000, 992476133, 992479633']

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(output_MMSI) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_MMSI, mode='w') as f:
    #    f.write(result.output)

    print('>> test_filter_field_MMSI #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 


def test_filter_field_callsign(tasks, data, output_callsign):
    filter_field._register(tasks)
    args = ['-v', '_parse', 'filter_field', 'callsign', '"FAD2407", "JY3726@"']

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(output_callsign) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_callsign, mode='w') as f:
    #    f.write(result.output)

    print('>> test_filter_field_callsign #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 


def test_filter_field_raim(tasks, data, output_raim):
    filter_field._register(tasks)
    args = ['-v', '_parse', 'filter_field', 'raim', 'True']

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(output_raim) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_raim, mode='w') as f:
    #    f.write(result.output)

    print('>> test_filter_field_raim #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 
