#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" filter AIS position messages according to a specified zone.

    The zone is specified by a list of points (longitude, latitude),
    which defines a polygon.

    Look for 'x' and 'y' fields in the AIS message, and test if the
    corresponding position is included in the specified zone. If it's
    included then the message is sent to next stage of the pipeline,
    otherwise it's ignored.

    If the message doesn't contain 'x' and 'y' fields then it's not
    processed and directly sent to next stage of the pipeline.

    .. seealso:: `usage`
"""

import logging
import click
import ast
from clichain import pipeline
from aistool.app import config, tasks
import sys
from functools import wraps
import matplotlib.path as mplPath
import numpy as np


logger = logging.getLogger(__name__)


def usage():
    return """ filter AIS position messages based on position.

        Input messages are expected to be a dict containing all the
        decoded fields.

        Filter AIS messages with position information ('x' and 'y'
        fields), according to a specified *zone* which is defined by
        a polygon (a list of points).

        The expected polygon is defined by an iterable of *points*,
        each *point* is defined itself by a couple **lon, lat**
        corresponding to **longitude** and **latitude** information (i.e
        'x' and 'y').

        .. note:: the polygon is automatically closed (the last point
            and the first point are joined)

        example: ::

            \b
            # the followings are equivalent:
            '(1, 2), (4, 5), (7, 8)'
            '(1,2), (4,5), (7,8)'
            '(1,2),(4,5),(7,8)'
            '((1, 2), (4, 5), (7, 8))'
            '[(1, 2), (4, 5), (7, 8)]'
            '[[1, 2], [4, 5], [7, 8]]'

        For every message containing 'x' and 'y' fields, test if the
        corresponding position is included in the expected polygon, if
        it's included then the message is sent to next stage of the
        pipeline, otherwise it's ignored.

        .. note:: will not filter messages not containing 'x' and 'y'
            fields, those messages will be sent to next stage of the
            pipeline.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='filter', help=usage())
    @click.argument('points')
    @wraps(cli)
    def _cli(points):
        return cli(points)

    return _cli


def cli(points):
    """ `click` command, see `usage` """
    try:
        pts = ast.literal_eval(points)
    except:
        raise click.BadParameter(f"bad value for 'points': {points}")

    poly = []
    for pt in pts:
        lon, lat = map(float, pt)
        poly.append([lon, lat])

    if len(poly) < 3:
        raise click.BadParameter(f"insufficient number of points: {points}")

    return filter_task(poly)


_register(tasks)


@pipeline.task
def filter_task(ctrl, poly):
    """ test if the message's position is included in the expected zone.

        .. note:: input data is expected to be a `dict` containing
            all decoded AIS fields.

        `poly` is a list containing all the **lon, lat** couples,
        which defines the expected zone.

        + if **'x'** and **'y'** fields are present in the message:

          test if **(x, y)** is included into `poly`. If it's included
          then send message to the next stage of the pipeline, if it's
          not then do nothing.

        + if the message does not contain **'x'** and **'y'** fields:

          the message is directly sent to the next stage of the pipeline.

        .. seealso:: `usage`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    _logger.info(f'filter zone: {poly}')

    poly = mplPath.Path(np.array(poly))
    contains = poly.contains_point

    with ctrl as push:
        while True:
            msg = yield

            try:
                xy = msg['x'], msg['y']
            except KeyError:
                # assume not a position message
                push(msg)

            if contains(xy):
                push(msg)

    _logger.info('Done.')
