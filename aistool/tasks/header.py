#!/usr/bin/env python
""" pre-process NMEA messages with header, keep NMEA only.

    process a header according to a specification (header format). The
    header format information is taken from the config file.

    The original message is expected to have the following form: ::

        <header><NMEA>

    The header format specification taken from the configuration file
    contains regex that will be used to split the header and the NMEA
    part, extract the timestamp from the header if any, and extra
    information / regex depending on the format type (to process
    multiline messages for example).

    The output will be a NMEA message with no header, and containing
    the timestamp **in epoch format** as last field if a timestamp has
    been extracted: ::

                                      |  | => <NMEA,timestamp>
        <header><NMEA> ===> process ==|or|
                                      |  | => <NMEA>

    .. note:: the timestamp will be **UTC** if the original timestamp
        is not in epoch format (i.e it has to be converted)

    You can see a list of the format types defined in the configuration
    file in the help message.

    .. seealso:: `usage`
"""

import logging
import click
import ast
from clichain import pipeline
from aistool.app import config, tasks
from datetime import datetime
import re
import sys
from functools import wraps
import pytz


logger = logging.getLogger(__name__)


header_options = ([k for k, v in config.items('header')] if
                  config.has_section('header') else [])


def usage():
    return f""" cut the header, extract timestamp and append to NMEA

        This task uses 'header' section in configuration: it should
        contain a dictionnary for each header format.

        You have to specify one header format to use this task. The
        header format is expected to contain following fields:

        NOTE: for more details on regex, see:
        https://docs.python.org/3/howto/regex.html (use "Groups"
        indicated with '(', ')' to capture specific part of the text)

        + 'split_header': this regex must **match** the whole header

        + 'capture_timestamp': this regex must **capture** the timestamp

        + 'timestamp_format': timestamp strftime format

          NOTE: you have to double the '%' char, for example:
          '%%d/%%m/%%y-%%H:%%M:%%S'

          (to see the full set of format codes supported on your
          platform, consult the strftime(3) documentation, or see
          https://docs.python.org/3/library/datetime.html)

        + 'ignore': (optional) match pattern to ignore specific lines

        + 'capture_multiline': this is used only by 'mares_dsi' format,
          it specifies how to extract information to process multiline
          messages: this regex must **capture**:

          \b
          1. the number of line (1,N)
          2. the total number of lines (2,N)
          3. the line ID

        The currently defined format are:

        {header_options}


        For each line:

        + skip line if empty or match the "ignore" regex

        + extract NMEA part using the "split_header" regex

          ( log an error and skip the line if any exception )

        + extract and parse timestamp part using the "capture_timestamp"
          and "timestamp_format" regex.

          ( silently ignore the header and keep NMEA part only if any
          exception )

        + send "NMEA,timestamp" or "NMEA" to next stage

        .. note:: the timestamp will be **UTC** if the original
            timestamp is not in epoch format (i.e it has to be converted)
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='header', help=usage())
    @click.argument('timestamp_format_type')
    @wraps(cli)
    def _cli(timestamp_format_type):
        return cli(timestamp_format_type)

    return _cli


def cli(timestamp_format_type):
    """ `click` command, see `usage` """
    try:
        fmt = config['header'][timestamp_format_type]
    except KeyError:
        raise click.BadParameter("unknown format type:"
                                 f"{timestamp_format_type}")

    try:
        ts_format = ast.literal_eval(fmt)
    except:
        raise click.BadParameter(f"incorrect format: "
                                 f"(type: {timestamp_format_type}): {fmt}")

    return header(timestamp_format_type, ts_format)


_register(tasks)


@pipeline.task
def header(ctrl, format_name, ts_format):
    """ 'header' coroutine implementation

        `ctrl` is the control object given to the coroutine function
        (see `clichain.pipeline.task` for details).

        `format_name` is the parameter provided to `cli`, specifying
        the header/timestamp format to use, it will be used as a key to
        determine the strategy to use to process lines.

        `ts_format` is the timestamp format used from the configuration
        file, it's a dictionnary:

        .. seealso:: `cli` for details

        for each line, remove any extra space before and after the line,
        ensure the line is non empty and the line doesn't match the
        `ignore` pattern.

        If the line is not skipped, apply the strategy to process each
        line.

        The strategy function is expected to return a tuple
        **(NMEA, ts)** where:

        + **NMEA** is the NMEA message (str)

        + **ts** is the timestamp in epoch format (**UTC**), or None

        If the strategy function returns a non empty value as **NMEA**
        then:

        + if **ts** is not None append the timestamp to NMEA (as last
          field of the message)

        + push the message to the next stage of the pipeline.

        If **NMEA** is empty then skip the line.
    """
    ignore = re.compile(ts_format['ignore']) if ts_format['ignore'] else None
    default = header._strategy.get(None)
    strategy = header._strategy.get(format_name, default)
    assert strategy, f"no registered strategy for '{format_name}' format"
    strategy = strategy(ctrl, ts_format)

    _logger = logger.getChild(ctrl.name)
    _logger.info(f'starting: timestamp format: \n{ts_format}')

    with ctrl as push:
        while True:
            line = yield

            # pre-process line
            line = line.strip()
            if line:
                if ignore and ignore.search(line):
                    continue
            else:
                continue

            # process message
            nmea, timestamp = strategy(line)

            # skip the line if could not extract header
            if nmea:
                # send only NMEA if no timestamp
                if timestamp:
                    push(f'{nmea},{timestamp}')
                else:
                    push(nmea)

    _logger.info(f'Done.')


def _strategy(format_name):
    """ decorator to register a strategy for `header`

        use `None` to register default strategy,
        otherwise use the name (str).
    """
    def register(f):
        header._strategy[format_name] = f
        return f
    return register


header._strategy = {}
header.strategy = _strategy


@header.strategy(None)  # None = default
def extract(ctrl, ts_format):
    """ create a function to extract header and timestamp from each line

        `ctrl` is the control object given to the coroutine function
        (see `clichain.pipeline.task` for details).

        `ts_format` is the timestamp format used from the configuration
        file, it's a dictionnary:

        .. seealso:: `cli` for details

        + use `ts_format` to split the header and NMEA part. If an error
          occurs then log the error and skip the line.

        + Then extract the timestamp from the header and append the
          timestamp to the NMEA data. If the timestamp is not in the
          expected format or cannot be extracted then ignore the header
          and keep NMEA part only.

        returns a tuple: **(NMEA, timestamp)** containing:

        + the NMEA message or None

        + the timestamp in epoch format or None.
    """
    cut_header = re.compile(ts_format['split_header'])
    get_timestamp = re.compile(ts_format['capture_timestamp'])
    dateformat = ts_format['timestamp_format']
    if (dateformat.strip() == '%s'):
        dateformat = None
    strptime = datetime.strptime
    _logger = logger.getChild(ctrl.name)
    utc = pytz.UTC

    def _extract(line):
        # extract NMEA from raw line
        try:
            nmea = cut_header.split(line, 1)[1]
            if not nmea:
                raise ValueError(line)
        except:
            _logger.error(f"could not extract NMEA from line: '{line}'")
            return None, None

        # extract timestamp and send "NMEA,timestamp"
        # send only NMEA if no timestamp or timestamp doesnt fit the
        # expected format.
        try:
            ts = get_timestamp.search(line).group(1)
            if dateformat:
                # get epoch in utc
                ts = strptime(ts, dateformat).replace(tzinfo=utc)
                ts = int(ts.timestamp())
            return nmea, ts
        except:
            return nmea, None

    return _extract


@header.strategy('mares_dsi')
def process_mares_DSI_multiline(ctrl, ts_format):
    """ `header` strategy for "mares_dsi" format

        .. seealso:: `header`

        Create a function to process multiline messages from "Mares/DSI"

        "Mares/DSI" uses a multiline header format, when multiline AIS
        messages are received the timestamp is only kept on the first
        line, and a "line ID" information is used to link all the lines
        of a same message.

        When processing those lines we maintain a cache of current
        timestamps, memomrizing the timestamp of a multiline message
        when reading its first line. A specific regex is used on each
        line to extract:

        1. the number of line (1,N)
        2. the total number of lines (2,N)
        3. the line ID

        It uses `extract` function to process the header.

        + before calling `extract` it captures the line number, number
          of lines in the group, and line ID.

          if it's the first line then the line contains the timestamp,
          so it will be memorized once the header has been extracted.

          .. note:: if an error occurs then consider it's a single line
            message and process it with `extract` (see following step).

        + extract the header and the timestamp if it's the first line,
          otherwise no timestamp will be extracted.

        + then if it's the first line in the group the timestamp is
          memorized in cache. If it's not then find the previously
          memorized timestamp in cache. If it's the last line then
          remove the timestamp from the cache.

        returns a tuple: **(NMEA, timestamp)** containing:

        + the NMEA message or None

        + the timestamp in epoch format or None.
    """
    _logger = logger.getChild(ctrl.name)
    _extract = extract(ctrl, ts_format)
    get_multiline = re.compile(ts_format['capture_multiline'])
    timestamps = {}  # cache

    def process_line(line):
        # pre process multiline messages
        srch = get_multiline.search(line)
        try:
            lineNbr = int(srch.group(1))
            groupSize = int(srch.group(2))
            lineID = srch.group(3)
        except:
            # assumes it's a single line message
            return _extract(line)

        # extract the header and timestamp if any
        nmea, ts = _extract(line)

        # post process multiline messages
        if lineNbr > 1:
            # not the first line :
            # use the timestamp got on the first line
            try:
                ts = timestamps[lineID]
            except KeyError:
                # log and return nmea only
                _logger.error(f"could not find timestamp for line: '{line}'"
                              " (it seems the first line was missing)")
                return nmea, None

            # clean cache if last line
            if lineNbr >= groupSize:
                del timestamps[lineID]

        # first line of the group: memorize the timestamp in cache
        else:
            timestamps[lineID] = ts

        return nmea, ts

    return process_line
