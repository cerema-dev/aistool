decode
======================

usage
----------------------------------------

usage: ::

    $ aistool decode --help

.. code-block:: none
    
    Usage: aistool decode [OPTIONS]
    
      decode AIS messages using `lpais.ais`
    
      AIS messages are decoded from NMEA messages, the line must start with '!',
      i.e any header must be removed before decoding.
    
      The input line should look like the following example: ::
    
      !AIVDM,1,1,,A,14h?D4002:PqoCpGCFVc0Hmh0<1H,0*0E
    
      If a line doesn't start with '!' it will be silently ignored.
    
      If the line cannot be decoded (an exception occurs) then the exception is
      logged and the line is ignored.
    
      If the line is correctly decoded then this produces a dictionary
      containing all the decoded fields.
    
    Options:
      -t, --aistypes TEXT  list of ais message types to keep: examples:
                           "1,2,3..."
                           "1, 2, 3..."
                           "(1,2,3...)"
                           "[1,2,3...]"
                           "1"
                           "1,"
                           
                           default value:
                           [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                           11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                           25]
      -k, --keepnmea       keep the original "nmea" message(s) in the data
                           (it
                           will be accessible "nmea" field in the dictionnary,
                           which will be a concatenation of original NMEA lines
                           in case of a multiline message)
      --help               Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.decode
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

