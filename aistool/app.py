#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" this module contains common interfaces for tasks implementation

    + `config`: `config` is a `configparser.ConfigParser` object, its
      contents depends on the contents of the configuration file.

      When the module is loaded, it will find and load the configuration
      file.

      the configuration file name is expected to be **aistool.conf**

      the configuration file will be searched from several possible
      paths, which are, in the order of priority:

      + AISTOOL_CONF environment variable
      + current local directory
      + User's home directory
      + /usr/local/etc/aistool

      If the configuration file could not be found, an error message is
      logged and a RuntimeError is raised.

      `config` is a `configparser.ConfigParser` object and its contents
      depends on the contents of the configuration file.

      .. seealso:: `load_config`, `configparser`

    + `tasks`: the factory to register tasks into, a
      `clichain.cli.Tasks` object

      .. seealso:: `clichain` for details

    This module is expected to be used in task implementations:

    example usage: ::

        from ..app import config, tasks
        import click
        from clichain import pipeline

        config = config['compute']


        @pipeline.task
        def compute(ctrl, param, option):
            [...]  # see 'clichain' documentation for details


        @tasks
        @click.command(name='compute')
        @click.option('--opt', default=config['opt'])
        @click.argument('param')
        def compute_cli(param, opt):
            "compute with param"
            return compte(param, opt)

    .. note:: see `clichain` documentation for details on how to
        implement tasks
"""

import configparser
import pathlib
import os
import logging
from clichain import cli


logger = logging.getLogger(__name__)


config_file = 'aistool.conf'
config_locations = [
    os.environ.get("AISTOOL_CONF", '.'),
    pathlib.Path('.').absolute(),
    pathlib.Path.home(),
    pathlib.Path('/usr/local/etc/aistool').absolute(),
    # pathlib.Path(__file__).parent,
]


config = configparser.ConfigParser()


def load_config(config_file=config_file, config_locations=config_locations):
    """ internal function to find and load the configuration file

        return True if the file has been found and loaded into `config`
    """
    logger.debug('searching for configuration file...')
    for path in map(pathlib.Path, config_locations):
        path = path / config_file
        logger.debug(f'looking for config file: "{path}"')
        if path.exists():
            logger.debug(f'"{path}" exists')
            if path.is_file():
                logger.debug(f'"{path}" is a file')
                logger.info(f'loading configuration file: "{path}"')
                with open(path) as conf:
                    config.read_file(conf)
                    logger.debug('Done.')
                    return True

    locations = '\n    -'.join(map(str, config_locations))
    msg = (f'couldnt find configuration file:\n'
           f'+ expected file name: {config_file}\n'
           f'+ expected file locations:\n    -{locations}')
    logger.error(msg)


def init(*args, **kw):
    """ called when the module is loaded, fails if no config file

        raise a RuntimeError if no configuration can be loaded.
    """
    if not load_config(*args, **kw):
        raise RuntimeError('could not find config file')


def clear_root_log_handlers():
    """ remove root log handlers before invoking the CLI.

        This function is called by `test` and `run` before starting
        the CLI. It removes all the handlers (see `logging.Handler`)
        attached to the root logger (i.e `logging.root`).

        This function has been added to ensure the logging configuration
        for `aistool` is done via CLI options (i.e the user options are
        respected).

        This function also ensures handlers are removed when running the
        CLI api several times in automated tests.
    """
    rl = logging.getLogger()
    rl.handlers.clear()


def test(clargs, inputs, args=None, kwargs=None, **kw):
    """ run the CLI using `clichain.cli.test`, intended for automated tests

        (`clichain.cli.test` uses `click.testing.CliRunner` to invoke
        the main command)

        .. seealso:: `run`

        + `clargs` is a list containing the command line arguments,
          i.e what the user would send in interactive mode.

        + `inputs` is an iterable containing the input data that would
          be send to stdin in interactive mode.

        + optional `args` and `kwargs` will be send to the `click`
          context (in context.obj['args'] and context.obj['kwargs'])

        + extra `kw` will be forwarded to
          `click.testing.CliRunner.invoke`, for example:

          ::

             catch_exceptions=False

        the function calls `clear_root_log_handlers` and then
        returns *clichain.cli.test* result.
    """
    clear_root_log_handlers()
    return cli.test(tasks, clargs,
                    args=() if args is None else args,
                    kwargs={} if kwargs is None else kwargs,
                    input=inputs,
                    **kw)


def run(*args, **kwargs):
    """ run the CLI, should be called once by main entry point

        .. seealso:: `test`

        `args` and `kwargs` will be send to the `click` context (in
        context.obj['args'] and context.obj['kwargs'])

        .. seealso:: `click`, `clichain.cli.app`

        the function calls `clear_root_log_handlers` and then
        calls *clichain.cli.app*.
    """
    clear_root_log_handlers()
    cli.app(tasks, *args, **kwargs)


tasks = cli.Tasks()
init()
