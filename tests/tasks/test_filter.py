#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.filter as filter_task
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_filter.txt'
    assert data.exists()
    return data


@pytest.fixture
def output():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_filter_out.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


def test_filter_help(tasks):
    filter_task._register(tasks)
    args = ['filter', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_filter_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], filter_task.usage()).ratio()
    assert ratio >= 0.8


def test_filter_invalid_value(tasks):
    filter_task._register(tasks)
    args = ['filter', 'foo']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_filter_invalid_value #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: bad value for 'points': foo" in result.output


def test_filter_invalid_polygon(tasks):
    filter_task._register(tasks)
    args = ['filter', '(1, 2), (3, 4)']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_filter_invalid_polygon #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: insufficient number of points: (1, 2), (3, 4)" in result.output


def test_filter_data(tasks, data, output):
    filter_task._register(tasks)
    args = ['-v', '_parse', 'filter', '(1, 41), (9, 41), (9, 50), (1, 50)']

    @pipeline.task
    def pre_process(ctrl):
        parse = ast.literal_eval
        with ctrl as push:
            while True:
                line = yield
                data = parse(line)
                push(data)

    @tasks
    @click.command(name='_parse')
    def _pre_process():
        return pre_process()

    with open(output) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output, mode='w') as f:
    #    f.write(result.output)

    print('>> test_filter_data #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)
    assert result.output == outputs 
