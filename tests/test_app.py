#!/usr/bin/env python

import aistool
import pytest
import logging
import sys
import aistool.app as app
import os


def test_config_load_example_file(caplog):
    caplog.set_level(logging.DEBUG)
    assert app.load_config()
    logs = '\n'.join(map(lambda r: r.message, caplog.records))
    assert logs == """\
searching for configuration file...
looking for config file: "../aistool.conf"
"../aistool.conf" exists
"../aistool.conf" is a file
loading configuration file: "../aistool.conf"
Done."""


def test_config_load_cant_find_file(caplog):
    caplog.set_level(logging.DEBUG)
    assert not app.load_config(config_file='foo',
                               config_locations=('bar', 'baz'))
    logs = '\n'.join(map(lambda r: r.message, caplog.records))
    assert logs == """\
searching for configuration file...
looking for config file: "bar/foo"
looking for config file: "baz/foo"
couldnt find configuration file:
+ expected file name: foo
+ expected file locations:
    -bar
    -baz"""


def test_import_config_no_config_file(caplog):
    caplog.set_level(logging.DEBUG)

    with pytest.raises(RuntimeError):
        app.init(config_file='foo', config_locations=('bar', 'baz'))
    
    logs = '\n'.join(map(lambda r: r.message, caplog.records))
    assert logs == """\
searching for configuration file...
looking for config file: "bar/foo"
looking for config file: "baz/foo"
couldnt find configuration file:
+ expected file name: foo
+ expected file locations:
    -bar
    -baz"""
