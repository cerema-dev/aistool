#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" store AIS data into a CSV file.

    .. note:: Input data is expected to be a `dict` containing decoded
        AIS fields.

    Store specific fields from input AIS messages into a CSV file.

    The CSV format can be controlled by several options, and a default
    value will be used to fill the blanks when a field cannot be found
    into the AIS message.

    .. seealso:: `usage`

    .. note:: assuming timezone is always UTC (if 'date' field)
"""

import logging
import click
import ast
from clichain import pipeline
from aistool.app import config, tasks
import sys
from functools import wraps
import csv
import pytz
import io


logger = logging.getLogger(__name__)


_datetime_format = config['csv']['default_datetime_format']
_delimiter = config['csv']['default_delimiter']
_quote_char = config['csv']['default_quote_char']
_quote_policy = config['csv']['default_quote_policy']
_value = config['csv']['default_value']


def usage():
    return """ store AIS fields into a CSV file.

        Select specific fields from decoded AIS data and store them into
        a CSV file.

        The expected fields are specified by an iterable of field names,
        for examples: ::

            \b
            # the followings are equivalent:
            '"x", "y"'
            '"x","y"
            '("x", "y")'
            '("x","y")'
            '["x", "y"]'
            '["x","y"]'

        A CSV column will be created for each selected field.
        If a field is not present into the AIS data then a default value
        is assigned.

        Several options allow to adjust the default value as well as the
        CSV format, default values for those options will be taken from
        the configuration file.

        Note the **'date'** field has to be formatted, an option allow
        to specify the expected output format. If the value is `None`
        it will not be formatted and the `None` value will be used
        instead.

        **NOTE**: assuming timezone is always UTC for **'date'** field.

        Input data is expected to be a `dict` containing decoded AIS
        fields.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='tocsv', help=usage())
    @click.argument('fields')
    @click.argument('csvfile', type=click.Path(exists=False))
    @click.option('--datetimeformat', '-t',
                  help="""strftime format for 'date' field.

                          To see the full set of format codes supported
                          on your platform, consult the strftime(3)
                          documentation, or see:
                          https://docs.python.org/library/datetime.html

                          If you want to convert to epoch timestamp then
                          use '%s'. Then the datetime information will
                          be converted into (UTC) epoch timestamp.

                          NOTE: in any other case you have to double the
                          '%' char, for example:
                          '%%d/%%m/%%y-%%H:%%M:%%S'""",
                  default=_datetime_format)
    @click.option('--field_delimiter', '-f',
                  help="csv delimiter",
                  default=_delimiter)
    @click.option('--quote_char', '-q',
                  help="csv quote char",
                  default=_quote_char)
    @click.option('--quote_policy', '-p',
                  help="csv quote policy: QUOTE_ALL / QUOTE_MINIMAL / "
                       "QUOTE_NONNUMERIC / QUOTE_NONE",
                  default=_quote_policy)
    @click.option('--default_value', '-d',
                  help="default value to use when a field is missing",
                  default=_value)
    @wraps(cli)
    def _cli(*args, **kw):
        return cli(*args, **kw)

    return _cli


def cli(fields,
        csvfile,
        datetimeformat,
        field_delimiter,
        quote_char,
        quote_policy,
        default_value):
    """ `click` command, see `usage` """
    try:
        fields = ast.literal_eval(fields)
    except:
        raise click.BadParameter(f"bad 'fields' value: {fields}")

    try:
        quote_policy = getattr(csv, quote_policy)
    except:
        raise click.BadParameter(f"bad 'quote_policy': {quote_policy}")

    kwargs = dict(restval=default_value,
                  delimiter=field_delimiter,
                  quotechar=quote_char,
                  quoting=quote_policy,
                  extrasaction='ignore',  # TODO: reason ?
                  lineterminator="\n")

    try:
        csv.DictWriter(io.StringIO(), [], **kwargs)
    except Exception as e:
        raise click.BadParameter(e)

    return tocsv(fields, csvfile, datetimeformat, kwargs)


_register(tasks)


@pipeline.task
def tocsv(ctrl,
          fields,
          csvfile,
          datetimeformat,
          kwargs):
    """ Open a CSV file and store specific fields into it.

        .. note:: input data is expected to be a `dict` containing
            all decoded AIS fields.

        `fields` is an iterable containing the fields to store into the
        CSV file, it will be passed to `csv.DictWriter`.

        `csvfile` is the path to the CSV file.

        Extra `kwargs` will be passed to `csv.DictWriter`.

        The **'date'** field will be formatted using `datetimeformat`,
        which specifies the **strftime** (`datetime.datetime.strftime`)
        format, or **'%s'** to use epoch timestamp. If the value is
        `None` it will not be formatted and the `None` value will be
        used instead.

        .. note:: assuming timezone is always UTC (if 'date' field)

        .. seealso:: `usage`
    """
    utc = pytz.UTC
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    _logger.info(f'fields: {fields}')
    _logger.info(f'csvfile: {csvfile}')
    _logger.info(f'datetime format: {datetimeformat}')
    _logger.info(f'CSV options: {kwargs}')

    with open(csvfile, 'w', newline='') as csvfile:
        # init csv file
        writer = csv.DictWriter(csvfile,
                                fieldnames=fields,
                                **kwargs)
        writer.writeheader()

        # start processing messages
        with ctrl as push:
            while True:
                msg = yield
                data = msg.copy()  # NOTE: shallow copy

                # process 'date' field if any
                try:
                    dt = data['date']
                    utc_dt = dt.replace(tzinfo=utc)
                    if datetimeformat == '%s':
                        data['date'] = int(utc_dt.timestamp())
                    else:
                        data['date'] = utc_dt.strftime(datetimeformat)
                except (KeyError, AttributeError):
                    # KeyError if not 'date' field
                    # AttributeError if date is None
                    pass

                # store data into csv file and send msg to next stage
                writer.writerow(data)
                push(msg)

    _logger.info('Done.')
