#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.decode as decode
from difflib import SequenceMatcher
import pathlib
import re


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_decode.raw'
    assert data.exists()
    return data


@pytest.fixture
def decoded():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_decode_decoded.txt'
    assert data.exists()
    return data


@pytest.fixture
def decoded_filter():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_decode_decoded_filter.txt'
    assert data.exists()
    return data


@pytest.fixture
def decoded_nmea():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_decode_decoded_nmea.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


def test_decode_help(tasks):
    decode._register(tasks)
    args = ['decode', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_decode_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], decode.usage()).ratio()
    assert ratio >= 0.8


def test_decode_invalid_types(tasks, data):
    decode._register(tasks)
    args = ['decode', '--aistypes', 'x']

    with open(data) as f:
        inputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_decode_invalid_types #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: incorrect value for aistypes: x" in result.output


def test_decode_no_option(tasks, data, decoded):
    decode._register(tasks)
    args = ['-v', 'decode']

    with open(data) as f:
        inputs = f.read()

    with open(decoded) as f:
        outputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_decode_no_option #1:\n', result.output, file=sys.stderr)
    assert result.output == outputs


def test_decode_filter_output(tasks, data, decoded_filter):
    decode._register(tasks)
    args = ['-v', 'decode', '--aistypes', '1, 5']

    with open(data) as f:
        inputs = f.read()

    with open(decoded_filter) as f:
        outputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_decode_filter_output #1:\n', result.output, file=sys.stderr)
    assert result.output == outputs


def test_decode_keep_nmea(tasks, data, decoded_nmea):
    decode._register(tasks)
    args = ['-v', 'decode', '--keepnmea']

    with open(data) as f:
        inputs = f.read()

    with open(decoded_nmea) as f:
        outputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_decode_keep_nmea #1:\n', result.output, file=sys.stderr)
    assert result.output == outputs
