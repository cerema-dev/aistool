aistool
======= 

.. toctree:: 
   :maxdepth: 2
   :caption: documentation:

   contents/doc.rst
   contents/tasks.rst
   contents/dev-doc.rst
   contents/aistool.rst


.. include:: ../../README.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
