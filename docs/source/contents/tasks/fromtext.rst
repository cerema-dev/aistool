fromtext
======================

usage
----------------------------------------

usage: ::

    $ aistool fromtext --help

.. code-block:: none

    Usage: aistool fromtext [OPTIONS]
    
      recreates AIS fields from string representation.
    
      usefull to reuse decoded AIS messages previously printed with no
      formatting (i.e the string representation of the dictionnary)
    
      each input message is expected to be the string representation of the
      dictionnary containing decoded AIS fields. It can also include the "date"
      field as it's added by *setdate* task.
    
      By default the "date" field will no be parsed and left as a string
      representation of the original value. This will provide better performance
      if no work is performed with this field. A specific flag can be set to
      turn on re-creation of the "date" field value.
    
      **NOTE**: If the "date" field is present then it's assumed to represent a
      UTC value.
    
      By default an error log will be emitted every time an input line cannot be
      parsed, this can be inhibited if the *silent* flag is set.
    
      Empty lines and lines beginning with "#" will not be processed.
    
    Options:
      -d, --date    parse "date" field if exists,
                    default will not attempt to
                    parse it and keep
                    its representation as the "date" field
                    value.
                    (this has no effect when no "date" field is
                    present in
                    the message).
      -s, --silent  prevent logging when an input cannot be
                    parsed, by default an
                    error level log will be
                    emitted.
      --help        Show this message and exit.


source documentation
----------------------------------------
.. automodule:: aistool.tasks.fromtext
    :ignore-module-all:
    :members:
    :private-members:
    :special-members: __init__, __getitem__, __setitem__, __delitem__, __len__, __call__, __enter__, __exit__

