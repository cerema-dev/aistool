#!/usr/bin/env python

import pytest
#import logging
import sys
from aistool import app


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


def test_import():
    from aistool import __main__


def test_logging(tasks):
    """ check the logging configuration works as expected (see issue #4)
    """
    inputs = '\n'

    args = ['[', ']']
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_logging #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
"""

    args = ['-v', '[', ']']
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_logging #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
clichain.cli: INFO: creating pipeline...
clichain.cli: INFO: processing...
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: DONE.
"""
