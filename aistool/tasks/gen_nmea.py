#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" outputs original NMEA message from decoded message

    inputs are decoded messages, the original NMEA data will be ouptut
    for messages containing a 'nmea' field.

    .. seealso:: `usage`

    .. note:: assuming timezone is always UTC ('date' field)
"""

import logging
import click
from clichain import pipeline
from aistool.app import config, tasks
from functools import wraps
import pytz


logger = logging.getLogger(__name__)


def usage():
    return """ outputs original NMEA message from decoded message.

        Input messages are expected to be a dict containing all the
        decoded fields.

        The input message must contain a 'nmea' field whose value is the
        original NMEA data, if it's a multiline message then 'nmea' is
        expected to be a concatenation of the original NMEA sentences,
        ex: ::

            '!AIVDM,2,1,[...]!AIVDM,2,2,[...]'

        in this case each original NMEA line will be output separately
        in the order in which they appear in 'nmea' field.

        Nothing is output if the decoded message does not contain a
        'nmea' field.

        It's possible to add a trailing timestamp field in NMEA message
        if the input message contains a (not None) 'date' field, using
        the appropriate option. The (epoch) timestamp field will be
        added only if the NMEA message does not already contain one.

        **NOTE**: assuming timezone is always UTC ('date' field)
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='gen_nmea', help=usage())
    @click.option('--settimestamp', '-t',
                  help="""use 'date' field to add a trailing timestamp
                          field to the NMEA message, only if the NMEA
                          message does not already contain one.
                          The datetime information contained into the
                          'date' field of the input data will be
                          converted to (UTC) epoch timestamp. Nothing
                          will be done if the input data does not
                          contain a 'date' field or that field is None.
                          The action will be done on each NMEA message
                          if the input data was decoded from multiple
                          NMEA lines.""",
                  is_flag=True)
    @wraps(cli)
    def _cli(settimestamp):
        return cli(settimestamp)

    return _cli


def cli(settimestamp):
    """ `click` command, see `usage` """
    return extract_nmea(settimestamp)


_register(tasks)


@pipeline.task
def extract_nmea(ctrl, settimestamp):
    """ Extract 'nmea' field data from input decoded messages.

        Input messages are expected to be a dict containing all the
        decoded fields.

        a new line is used for each NMEA message, either for single line
        messages or multiline messages (i.e decoded from several NMEA
        messages).

        If `settimestamp` is `True`, an epoch timestamp will be added to
        each NMEA message if:

        + the input data contains a 'date' field (and it's not `None`)

          which in this case should be a `datetime.datetime` object

        + the NMEA message does not already contain a trailing
          timestamp field (the timestamp field is always the last field)

        .. note:: the same timestamp value will be used for all NMEA
            messages in the case of multiline messages.

        .. seealso:: `usage`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    if settimestamp:
        _logger.info(f'use "date" field to add timestamp if no timestamp')
        utc = pytz.UTC
    else:
        _logger.info(f'dont try to add timestamp')

    with ctrl as push:
        while True:
            msg = yield
            try:
                origin = msg['nmea']
            except KeyError:
                continue

            header = origin.split(',', maxsplit=1)[0]   # ex: !AIVDM
            # A message can be decoded from several NMEA lines...
            # In this case the 'nmea' field is a concatenation, ex:
            # '!AIVDM,2,1,[...]!AIVDM,2,2,[...]'
            nmea_lst = origin.split(header)
            for nmea in nmea_lst:
                nmea = nmea.strip()  # remove the '\n' at the end  XXX
                if nmea:  # first item in the list will be empty ('')

                    # add timestamp if requested and:
                    # - if 'date' field and not None
                    # - if no timestamp yet
                    if settimestamp:
                        date = msg.get('date')
                        if date is not None:
                            try:
                                # following will succeed if timestamp exists
                                float(nmea.split(',')[-1])
                            except ValueError:
                                utc_dt = date.replace(tzinfo=utc)
                                nmea = f'{nmea},{int(utc_dt.timestamp())}'

                    # send to next stage
                    push(f'{header},{nmea}')

    _logger.info('Done.')
