User's documentation
=====================

.. seealso:: `aistool` is based on `clichain`


install
----------------------------------------

You need **Python >= 3.6** to be able to install `aistool`.

To install `aistool` first download or clone `the repository`_, then
go to the repo's root directory and use the makefile: 

.. code-block:: bash

    $ make install

.. warning:: user is encouraged to use `gcc`_ to compile dependencies
    (*gcc* and *g++*), otherwise it's likely to fail. Make sure **gcc**
    will be used as default compliler or specify it when install / build

    example:

    .. code-block:: bash

        $ CC=gcc-8 CXX=g++-8 make install

.. note:: The best way of getting the source is to use `git`_ to clone
    the repository. You might have to install git on your operating
    system at first.

This will install the tool into the current python environment. The user
is advised to use a virtual environment. The creation of the virtual
environment may vary according to the operating system you're using.
The easiest way is normally to use Python's builtin `venv` module, which
comes with Python 3.

Installation example using git and a virtual environment:

.. code-block:: bash

    $ cd
    $ mkdir aistool && cd aistool
    $ python3 --version
    Python 3.6.4
    $ python3 -m venv venv
    $ source venv/bin/activate
    (venv) $ python --version
    Python 3.6.4
    (venv) $ git clone https://framagit.org/cerema-dev/aistool.git
    (venv) $ Cloning into 'aistool'...
    [...]
    (venv) $ pip install --upgrade pip
    (venv) $ cd aistool && make install

.. note:: once installed in a virtual environment you'll have to
    activate that `venv` when you want to use it.

.. _configuration_file:

Configuration file
************************

`aistool` needs a configuration file to run.

When running the command line interface, `aistool` will look for several
possible paths to find the configuration file whose name is expected to
be **aistool.conf**.

The expected paths are, in order of priority:

+ **AISTOOL_CONF** environment variable

+ current local directory ('.')

+ User's home directory

+ /usr/local/etc/aistool

An error message will indicate the expected paths if the configuration
file could not be found.

You will find an example configuration file in the repository's root
directory, called **aistool.conf**. Copy it to the desired
location if you don't have a configuration yet when installing.

example:

.. code-block:: bash

    [...]
    $ cp aistool/aistool.conf ~/

The syntax of the configuration file is straightforward as it's a basic
python configuration file organized by sections.

example: ::

    [DEFAULT]
    ServerAliveInterval = 45
    Compression = yes
    CompressionLevel = 9
    ForwardX11 = yes
    
    [bitbucket.org]
    User = hg
    
    [topsecret.server.com]
    Port = 50022
    ForwardX11 = no

usage
----------------------------------------

`aistool` is a command line interface tool.

It's run using **aistool** command:

.. code-block:: bash

    $ aistool --help
    Usage: aistool [OPTIONS] COMMAND1 [ARGS]... [COMMAND2 [ARGS]...]...

      create a pipeline of tasks, read text data from the standard input
      and send results to the standard output: ::
    
                  stdin(text) --> tasks... --> stdout(text)
    
    [...]

When executed, it will read (text) data from standard input and print
any output data to standard output.

example: we have a file "**sample.raw**" containing raw NMEA lines in the
current directory:

.. code-block:: bash
    
    $ head sample.raw 
    
    !AIVDM,1,1,,A,14h?D4002:PqoCpGCFVc0Hmh0<1H,0*0E
    !AIVDM,1,1,,B,B3HV>iP000:1RnV5e=EjwwuUoP06,0*47
    !AIVDM,1,1,,A,ENjAavjS0a7h22h1PQ7h1a2baP0@7Uf@<6pP000003P02P,4*0B
    !AIVDM,1,1,,A,ENjAaw2S0a7h8:W:0h9Ra72V60P@7BnL<7NSH00003P02P,4*3B
    !AIVDM,1,1,,A,H3HmiWTT1=30000614jlho000000,0*4B
    !AIVDM,2,1,9,A,53Ho19P1sik8H4CCC75A<n0P4q>222222222220l0`C636d>0:TiE1H88888,0*0F
    !AIVDM,2,2,9,A,88888888880,2*2D
    !AIVDM,1,1,,A,6>jCL@P0000<>db4@104020,2*2E
    !AIVDM,1,1,,A,6>jCL@P0000<>db4@104020,2*2E

We can see the available commands at the end of **aistool** help:

.. code-block:: bash

    $ aistool --help | sed -n '/Commands:/,$ p'
    Commands:
      decode        decode AIS messages using `lpais.ais` AIS messages are...
      file          Writes data into the specified file as text.
      filter        filter AIS position messages based on position.
      filter_field  filter messages based on a specific field.
      fromtext      recreates AIS fields from string representation.
      gen_nmea      outputs original NMEA message from decoded message.
      header        cut the header, extract timestamp and append to NMEA This...
      setdate       add 'date' field to AIS data.
      tocsv         store AIS fields into a CSV file.
      [             begin fork
      ]             end fork
      ,             new branch
      {             begin debug name group
      }             end debug name group

We can get information about the **decode** task using **--help** option:

.. code-block:: bash
    
    $ aistool decode --help
    Usage: aistool decode [OPTIONS]
    
      decode AIS messages using `lpais.ais`
    
      AIS messages are decoded from NMEA messages, the line must start with '!',
      i.e any header must be removed before decoding.
    
      The input line should look like the following example: ::
    
      !AIVDM,1,1,,A,14h?D4002:PqoCpGCFVc0Hmh0<1H,0*0E
    
      If a line doesn't start with '!' it will be silently ignored.
    
      If the line cannot be decoded (an exception occurs) then the exception is
      logged and the line is ignored.
    
      If the line is correctly decoded then this produces a dictionary
      containing all the decoded fields.
    
    Options:
      -t, --aistypes TEXT  list of ais message types to keep: examples:
                           "1,2,3..."
                           "1, 2, 3..."
                           "(1,2,3...)"
                           "[1,2,3...]"
                           "1"
                           "1,"
                           
                           default value:
                           [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                           11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                           25]
      -k, --keepnmea       keep the original "nmea" message(s) in the data
                           (it
                           will be accessible "nmea" field in the dictionnary,
                           which will be a concatenation of original NMEA lines
                           in case of a multiline message)
      --help               Show this message and exit.

Let's decode the content of the "**sample.raw**" file:

(*the decoded messages come up in the standard output,
we only keep the 5 first lines in this example*)

.. code-block:: bash
    
    $ cat sample.raw | aistool decode | head -5
    {'id': 1, 'repeat_indicator': 0, 'mmsi': 319018000, 'nav_status': 0, 'rot_over_range': False, 'rot': 0.0, 'sog': 13.800000190734863, 'position_accuracy': 1, 'x': 12.640633333333334, 'y': 40.72388333333333, 'cog': 281.70001220703125, 'true_heading': 282, 'timestamp': 56, 'special_manoeuvre': 0, 'spare': 0, 'raim': False, 'sync_state': 0, 'slot_timeout': 3, 'received_stations': 88}
    {'id': 18, 'repeat_indicator': 0, 'mmsi': 227118790, 'spare': 0, 'sog': 0.0, 'position_accuracy': 0, 'x': 8.759221666666667, 'y': 42.56632833333333, 'cog': 183.89999389648438, 'true_heading': 511, 'timestamp': 59, 'spare2': 0, 'unit_flag': 1, 'display_flag': 0, 'dsc_flag': 1, 'band_flag': 1, 'm22_flag': 1, 'mode_flag': 0, 'raim': True, 'commstate_flag': 1, 'commstate_cs_fill': 393222}
    {'id': 21, 'repeat_indicator': 1, 'mmsi': 992242171, 'spare': 0, 'aton_type': 5, 'name': 'FARO DE CABO CREUS@@', 'position_accuracy': 1, 'x': 3.3158666666666665, 'y': 42.318933333333334, 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'fix_type': 7, 'timestamp': 0, 'off_pos': False, 'aton_status': 0, 'raim': True, 'virtual_aton': False, 'assigned_mode': True}
    {'id': 21, 'repeat_indicator': 1, 'mmsi': 992242172, 'spare': 0, 'aton_type': 5, 'name': 'FARO PUNTA SERNELLA@', 'position_accuracy': 1, 'x': 3.1870333333333334, 'y': 42.351405, 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'fix_type': 7, 'timestamp': 0, 'off_pos': False, 'aton_status': 0, 'raim': True, 'virtual_aton': False, 'assigned_mode': True}
    {'id': 24, 'repeat_indicator': 0, 'mmsi': 227373470, 'part_num': 1, 'type_and_cargo': 36, 'vendor_id': 'AMC@@@@', 'callsign': 'FAD2407', 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'spare': 0}
    
We use the options to only keep AIS 1, 2, 3 messages:

.. code-block:: bash

    $ cat sample.raw | aistool decode --aistypes 1,2,3
    {'id': 1, 'repeat_indicator': 0, 'mmsi': 319018000, 'nav_status': 0, 'rot_over_range': False, 'rot': 0.0, 'sog': 13.800000190734863, 'position_accuracy': 1, 'x': 12.640633333333334, 'y': 40.72388333333333, 'cog': 281.70001220703125, 'true_heading': 282, 'timestamp': 56, 'special_manoeuvre': 0, 'spare': 0, 'raim': False, 'sync_state': 0, 'slot_timeout': 3, 'received_stations': 88}
    {'id': 1, 'repeat_indicator': 0, 'mmsi': 310610000, 'nav_status': 0, 'rot_over_range': False, 'rot': 0.0, 'sog': 19.100000381469727, 'position_accuracy': 1, 'x': 10.329571666666666, 'y': 42.55013, 'cog': 307.70001220703125, 'true_heading': 308, 'timestamp': 0, 'special_manoeuvre': 0, 'spare': 0, 'raim': False, 'sync_state': 0, 'slot_timeout': 1, 'utc_hour': 22, 'utc_min': 0, 'utc_spare': 0}

.. note:: we can see we have two messages in this example, each decoded
    message produced by *decode* is a mapping of **key: value** pairs...
    
Now we want to chain the decoded messages with the task **filter**:

.. code-block:: bash
    
    $ cat sample.raw | aistool -vv decode -t '1,2,3, 5, 18,19, 21' filter --help
    Usage: aistool filter [OPTIONS] POINTS
    
      filter AIS position messages based on position.
    
      Input messages are expected to be a dict containing all the decoded
      fields.
    
      Filter AIS messages with position information ('x' and 'y' fields),
      according to a specified *zone* which is defined by a polygon (a list of
      points).
    
      The expected polygon is defined by an iterable of *points*, each *point*
      is defined itself by a couple **lon, lat** corresponding to **longitude**
      and **latitude** information (i.e 'x' and 'y').
    
      .. note:: the polygon is automatically closed (the last point     and the
      first point are joined)
    
      example: ::
    
              # the followings are equivalent:
              '(1, 2), (4, 5), (7, 8)'
              '(1,2), (4,5), (7,8)'
              '(1,2),(4,5),(7,8)'
              '((1, 2), (4, 5), (7, 8))'
              '[(1, 2), (4, 5), (7, 8)]'
              '[[1, 2], [4, 5], [7, 8]]'
    
      For every message containing 'x' and 'y' fields, test if the corresponding
      position is included in the expected polygon, if it's included then the
      message is sent to next stage of the pipeline, otherwise it's ignored.
    
      .. note:: will not filter messages not containing 'x' and 'y'     fields,
      those messages will be sent to next stage of the     pipeline.
    
    Options:
      --help  Show this message and exit.
    
We want to specify the following area: lon in [0, 15] and lat in [42, 43]:

.. code-block:: bash

    $ cat sample.raw | aistool decode -t '1,2,3, 5, 18,19, 21' filter "(0, 42), (15, 42), (15, 43), (0, 43)" | wc -l
      13
    
    # we got 13 messages

Now using **filter_field** we want to chain the results into two branches:

+ one keeping the messages with a **'raim'** equal to `True`

+ one keeping the messages with only the **'mmsi'** values: **227118790**, **375199000**, **310610000**

First we use variables to get things more readable for the example:

.. code-block:: bash

    $ export ZONE="(0, 42), (15, 42), (15, 43), (0, 43)"
    $ export AIS_TYPES='1,2,3, 5, 18,19, 21'

The **filter_field** task:

.. code-block:: bash

    $ cat sample.raw | aistool decode -t "$AIS_TYPES" filter "$ZONE" filter_field --help
    Usage: aistool filter_field [OPTIONS] FIELD VALUES
    
      filter messages based on a specific field.
    
      decoded messages are filtered by comparing a specific field to one or
      several accepted values.
    
      input messages are expected to be a dict containing all the decoded
      fields.
    
      **FIELD** specifies which field to compare in the message data.
    
      **VALUES** specifies the accepted values for the specified field. It can
      be either a single value or a sequence of values, example: ::
    
              # single value:
              '319018000'
              '"FAD2407"'
              'True'
    
              # the followings are equivalent:
              '227393830, 319018000, 992476133, 992479633'
              '[227393830, 319018000, 992476133, 992479633]'
              '(227393830, 319018000, 992476133, 992479633)'
              '(227393830,319018000,992476133,992479633)'
    
      **NOTE**: accepted value(s) must be basic types such as integer or
      strings, True/False are also accepted.
    
    Options:
      --help  Show this message and exit.


Let's try the first branch:

.. code-block:: bash

    $ cat sample.raw | aistool decode -t "$AIS_TYPES" filter "$ZONE" filter_field "raim" True
    {'id': 18, 'repeat_indicator': 0, 'mmsi': 227118790, 'spare': 0, 'sog': 0.0, 'position_accuracy': 0, 'x': 8.759221666666667, 'y': 42.56632833333333, 'cog': 183.89999389648438, 'true_heading': 511, 'timestamp': 59, 'spare2': 0, 'unit_flag': 1, 'display_flag': 0, 'dsc_flag': 1, 'band_flag': 1, 'm22_flag': 1, 'mode_flag': 0, 'raim': True, 'commstate_flag': 1, 'commstate_cs_fill': 393222}
    {'id': 21, 'repeat_indicator': 1, 'mmsi': 992242171, 'spare': 0, 'aton_type': 5, 'name': 'FARO DE CABO CREUS@@', 'position_accuracy': 1, 'x': 3.3158666666666665, 'y': 42.318933333333334, 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'fix_type': 7, 'timestamp': 0, 'off_pos': False, 'aton_status': 0, 'raim': True, 'virtual_aton': False, 'assigned_mode': True}
    {'id': 21, 'repeat_indicator': 1, 'mmsi': 992242172, 'spare': 0, 'aton_type': 5, 'name': 'FARO PUNTA SERNELLA@', 'position_accuracy': 1, 'x': 3.1870333333333334, 'y': 42.351405, 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'fix_type': 7, 'timestamp': 0, 'off_pos': False, 'aton_status': 0, 'raim': True, 'virtual_aton': False, 'assigned_mode': True}


Let's try the second branch:

.. code-block:: bash

    $ cat sample.raw | aistool decode -t "$AIS_TYPES" filter "$ZONE" filter_field "mmsi" "227118790, 375199000, 310610000"
    {'id': 18, 'repeat_indicator': 0, 'mmsi': 227118790, 'spare': 0, 'sog': 0.0, 'position_accuracy': 0, 'x': 8.759221666666667, 'y': 42.56632833333333, 'cog': 183.89999389648438, 'true_heading': 511, 'timestamp': 59, 'spare2': 0, 'unit_flag': 1, 'display_flag': 0, 'dsc_flag': 1, 'band_flag': 1, 'm22_flag': 1, 'mode_flag': 0, 'raim': True, 'commstate_flag': 1, 'commstate_cs_fill': 393222}
    {'id': 5, 'repeat_indicator': 0, 'mmsi': 375199000, 'ais_version': 0, 'imo_num': 0, 'callsign': 'JY3726@', 'name': 'EOL B@@@@@@@@@@@@@@@', 'type_and_cargo': 37, 'dim_a': 24, 'dim_b': 10, 'dim_c': 7, 'dim_d': 1, 'fix_type': 1, 'eta_month': 0, 'eta_day': 0, 'eta_hour': 24, 'eta_minute': 60, 'draught': 1.2999999523162842, 'destination': 'ELBE@@@@@@@@@@@@@@@@', 'dte': 0, 'spare': 0}
    {'id': 5, 'repeat_indicator': 0, 'mmsi': 375199000, 'ais_version': 0, 'imo_num': 0, 'callsign': 'JY3726@', 'name': 'EOL B@@@@@@@@@@@@@@@', 'type_and_cargo': 37, 'dim_a': 24, 'dim_b': 10, 'dim_c': 7, 'dim_d': 1, 'fix_type': 1, 'eta_month': 0, 'eta_day': 0, 'eta_hour': 24, 'eta_minute': 60, 'draught': 1.2999999523162842, 'destination': 'ELBE@@@@@@@@@@@@@@@@', 'dte': 0, 'spare': 0}
    {'id': 1, 'repeat_indicator': 0, 'mmsi': 310610000, 'nav_status': 0, 'rot_over_range': False, 'rot': 0.0, 'sog': 19.100000381469727, 'position_accuracy': 1, 'x': 10.329571666666666, 'y': 42.55013, 'cog': 307.70001220703125, 'true_heading': 308, 'timestamp': 0, 'special_manoeuvre': 0, 'spare': 0, 'raim': False, 'sync_state': 0, 'slot_timeout': 1, 'utc_hour': 22, 'utc_min': 0, 'utc_spare': 0}

Finally we reuse the common trunk results in two branches:

.. code-block:: bash

    $ export MMSI="227118790, 375199000, 310610000"
    $ cat sample.raw | aistool decode -t "$AIS_TYPES" filter "$ZONE" [ filter_field "raim" True , filter_field "mmsi" "$MMSI" ]
    {'id': 18, 'repeat_indicator': 0, 'mmsi': 227118790, 'spare': 0, 'sog': 0.0, 'position_accuracy': 0, 'x': 8.759221666666667, 'y': 42.56632833333333, 'cog': 183.89999389648438, 'true_heading': 511, 'timestamp': 59, 'spare2': 0, 'unit_flag': 1, 'display_flag': 0, 'dsc_flag': 1, 'band_flag': 1, 'm22_flag': 1, 'mode_flag': 0, 'raim': True, 'commstate_flag': 1, 'commstate_cs_fill': 393222}
    {'id': 18, 'repeat_indicator': 0, 'mmsi': 227118790, 'spare': 0, 'sog': 0.0, 'position_accuracy': 0, 'x': 8.759221666666667, 'y': 42.56632833333333, 'cog': 183.89999389648438, 'true_heading': 511, 'timestamp': 59, 'spare2': 0, 'unit_flag': 1, 'display_flag': 0, 'dsc_flag': 1, 'band_flag': 1, 'm22_flag': 1, 'mode_flag': 0, 'raim': True, 'commstate_flag': 1, 'commstate_cs_fill': 393222}
    {'id': 21, 'repeat_indicator': 1, 'mmsi': 992242171, 'spare': 0, 'aton_type': 5, 'name': 'FARO DE CABO CREUS@@', 'position_accuracy': 1, 'x': 3.3158666666666665, 'y': 42.318933333333334, 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'fix_type': 7, 'timestamp': 0, 'off_pos': False, 'aton_status': 0, 'raim': True, 'virtual_aton': False, 'assigned_mode': True}
    {'id': 21, 'repeat_indicator': 1, 'mmsi': 992242172, 'spare': 0, 'aton_type': 5, 'name': 'FARO PUNTA SERNELLA@', 'position_accuracy': 1, 'x': 3.1870333333333334, 'y': 42.351405, 'dim_a': 0, 'dim_b': 0, 'dim_c': 0, 'dim_d': 0, 'fix_type': 7, 'timestamp': 0, 'off_pos': False, 'aton_status': 0, 'raim': True, 'virtual_aton': False, 'assigned_mode': True}
    {'id': 5, 'repeat_indicator': 0, 'mmsi': 375199000, 'ais_version': 0, 'imo_num': 0, 'callsign': 'JY3726@', 'name': 'EOL B@@@@@@@@@@@@@@@', 'type_and_cargo': 37, 'dim_a': 24, 'dim_b': 10, 'dim_c': 7, 'dim_d': 1, 'fix_type': 1, 'eta_month': 0, 'eta_day': 0, 'eta_hour': 24, 'eta_minute': 60, 'draught': 1.2999999523162842, 'destination': 'ELBE@@@@@@@@@@@@@@@@', 'dte': 0, 'spare': 0}
    {'id': 5, 'repeat_indicator': 0, 'mmsi': 375199000, 'ais_version': 0, 'imo_num': 0, 'callsign': 'JY3726@', 'name': 'EOL B@@@@@@@@@@@@@@@', 'type_and_cargo': 37, 'dim_a': 24, 'dim_b': 10, 'dim_c': 7, 'dim_d': 1, 'fix_type': 1, 'eta_month': 0, 'eta_day': 0, 'eta_hour': 24, 'eta_minute': 60, 'draught': 1.2999999523162842, 'destination': 'ELBE@@@@@@@@@@@@@@@@', 'dte': 0, 'spare': 0}
    {'id': 1, 'repeat_indicator': 0, 'mmsi': 310610000, 'nav_status': 0, 'rot_over_range': False, 'rot': 0.0, 'sog': 19.100000381469727, 'position_accuracy': 1, 'x': 10.329571666666666, 'y': 42.55013, 'cog': 307.70001220703125, 'true_heading': 308, 'timestamp': 0, 'special_manoeuvre': 0, 'spare': 0, 'raim': False, 'sync_state': 0, 'slot_timeout': 1, 'utc_hour': 22, 'utc_min': 0, 'utc_spare': 0}

.. note:: The results are concatenated in stdout here but ususally you'll use **file** process in each branch to
    keep the results in a specific file...

We can put as many tasks as we want in a branch, for example we can add a **gen_nmea** task in the first branch:

.. code-block:: bash

    $ cat sample.raw | aistool decode -t "$AIS_TYPES" --keepnmea filter "$ZONE" [ filter_field "raim" True gen_nmea , filter_field "mmsi" "$MMSI"]
    !AIVDM,,1,1,,B,B3HV>iP000:1RnV5e=EjwwuUoP06,0*47
    {'id': 18, 'repeat_indicator': 0, 'mmsi': 227118790, 'spare': 0, 'sog': 0.0, 'position_accuracy': 0, 'x': 8.759221666666667, 'y': 42.56632833333333, 'cog': 183.89999389648438, 'true_heading': 511, 'timestamp': 59, 'spare2': 0, 'unit_flag': 1, 'display_flag': 0, 'dsc_flag': 1, 'band_flag': 1, 'm22_flag': 1, 'mode_flag': 0, 'raim': True, 'commstate_flag': 1, 'commstate_cs_fill': 393222, 'nmea': '!AIVDM,1,1,,B,B3HV>iP000:1RnV5e=EjwwuUoP06,0*47'}
    !AIVDM,,1,1,,A,ENjAavjS0a7h22h1PQ7h1a2baP0@7Uf@<6pP000003P02P,4*0B
    !AIVDM,,1,1,,A,ENjAaw2S0a7h8:W:0h9Ra72V60P@7BnL<7NSH00003P02P,4*3B
    {'id': 5, 'repeat_indicator': 0, 'mmsi': 375199000, 'ais_version': 0, 'imo_num': 0, 'callsign': 'JY3726@', 'name': 'EOL B@@@@@@@@@@@@@@@', 'type_and_cargo': 37, 'dim_a': 24, 'dim_b': 10, 'dim_c': 7, 'dim_d': 1, 'fix_type': 1, 'eta_month': 0, 'eta_day': 0, 'eta_hour': 24, 'eta_minute': 60, 'draught': 1.2999999523162842, 'destination': 'ELBE@@@@@@@@@@@@@@@@', 'dte': 0, 'spare': 0, 'nmea': '!AIVDM,2,1,1,A,55UlE6000000aW?O;H0Dtj08000000000000000U30:7140Ht3AC0Q@00000,0*4B!AIVDM,2,2,1,A,00000000000,2*25'}
    {'id': 5, 'repeat_indicator': 0, 'mmsi': 375199000, 'ais_version': 0, 'imo_num': 0, 'callsign': 'JY3726@', 'name': 'EOL B@@@@@@@@@@@@@@@', 'type_and_cargo': 37, 'dim_a': 24, 'dim_b': 10, 'dim_c': 7, 'dim_d': 1, 'fix_type': 1, 'eta_month': 0, 'eta_day': 0, 'eta_hour': 24, 'eta_minute': 60, 'draught': 1.2999999523162842, 'destination': 'ELBE@@@@@@@@@@@@@@@@', 'dte': 0, 'spare': 0, 'nmea': '!AIVDM,2,1,1,A,55UlE6000000aW?O;H0Dtj08000000000000000U30:7140Ht3AC0Q@00000,0*4B!AIVDM,2,2,1,A,00000000000,2*25'}
    {'id': 1, 'repeat_indicator': 0, 'mmsi': 310610000, 'nav_status': 0, 'rot_over_range': False, 'rot': 0.0, 'sog': 19.100000381469727, 'position_accuracy': 1, 'x': 10.329571666666666, 'y': 42.55013, 'cog': 307.70001220703125, 'true_heading': 308, 'timestamp': 0, 'special_manoeuvre': 0, 'spare': 0, 'raim': False, 'sync_state': 0, 'slot_timeout': 1, 'utc_hour': 22, 'utc_min': 0, 'utc_spare': 0, 'nmea': '!AIVDM,1,1,,A,14`>8D002wPgB?NHF>od1I`006h0,0*0D'}


.. _the repository: https://framagit.org/cerema-dev/aistool.git
.. _git: https://git-scm.com/
.. _gcc: https://gcc.gnu.org/
