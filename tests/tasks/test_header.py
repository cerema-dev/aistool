#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.header as header
from difflib import SequenceMatcher
import pathlib


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_header.raw'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


def test_header_help(tasks):
    header._register(tasks)
    args = ['header', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_header_help #1:\n', result.output, file=sys.stderr)
    ratio = SequenceMatcher(None, result.output, header.usage()).ratio()
    assert ratio >= 0.8


def test_header_invalid_format(tasks, data):
    header._register(tasks)
    args = ['header', 'x']

    with open(data) as f:
        inputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_header #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: unknown format type:x" in result.output


def test_header_mares_dsi(tasks, data):
    header._register(tasks)
    args = ['-v', 'header', 'mares_dsi']

    with open(data) as f:
        inputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_header_mares_dsi #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
clichain.cli: INFO: create: header
clichain.cli: INFO: creating pipeline...
aistool.tasks.header.0: INFO: starting: timestamp format: 
{'split_header': '[^!]+', 'capture_timestamp': '^\\\\\\\\(?:1G\\\\d+:\\\\d+,)?c:(\\\\d+)', 'timestamp_format': '%s', 'ignore': None, 'capture_multiline': '^\\\\\\\\(\\\\d+)G(\\\\d+):(\\\\d+)'}
clichain.cli: INFO: processing...
clichain.cli: INFO: ----------------------------------------
!ABVDM,1,1,,A,18J6uh05QpwCM=hHrGK12hn00<13,0*40,1483912803
!ABVDM,1,1,,A,13E`ca001VwIcDNHn>sMgJon28S8,0*53,1483912803
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23,1483912800
!ABVDM,2,2,7,A,00000000000,2*28,1483912800
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23,1483912800
!ABVDM,2,2,7,A,00000000000,2*28,1483912800
!ABVDM,2,2,7,A,00000000000,2*28,1483912800
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72,1483912804
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# sat ais alg'
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# antilles guyane'
!KWVDM,1,1,2,B,1815De@02@LE8K42qtTd6aP406h0,0*09
aistool.tasks.header.0: ERROR: could not extract NMEA from line: 'PAS ANALYSE : \s:PSS_Kaw,c:1480370235*63\$KWVSI,ELM599,2,220003.337800,125,,*07'
aistool.tasks.header.0: ERROR: could not find timestamp for line: '\\2G3:1702*72\!ABVDM,2,2,7,A,00000000000,2*28' (it seems the first line was missing)
!ABVDM,2,2,7,A,00000000000,2*28
aistool.tasks.header.0: INFO: Done.
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: DONE.
"""


def test_header_northsea_dsi(tasks, data):
    header._register(tasks)
    args = ['-v', 'header', 'northsea_dsi']

    with open(data) as f:
        inputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_header_northsea_dsi #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
clichain.cli: INFO: create: header
clichain.cli: INFO: creating pipeline...
aistool.tasks.header.0: INFO: starting: timestamp format: 
{'split_header': '[^!]+', 'capture_timestamp': '^\\\\\\\\c:(\\\\d+)', 'timestamp_format': '%s', 'ignore': None}
clichain.cli: INFO: processing...
clichain.cli: INFO: ----------------------------------------
!ABVDM,1,1,,A,18J6uh05QpwCM=hHrGK12hn00<13,0*40,1483912803
!ABVDM,1,1,,A,13E`ca001VwIcDNHn>sMgJon28S8,0*53,1483912803
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23
!ABVDM,2,2,7,A,00000000000,2*28
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23
!ABVDM,2,2,7,A,00000000000,2*28
!ABVDM,2,2,7,A,00000000000,2*28
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72,1483912804
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# sat ais alg'
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# antilles guyane'
!KWVDM,1,1,2,B,1815De@02@LE8K42qtTd6aP406h0,0*09
aistool.tasks.header.0: ERROR: could not extract NMEA from line: 'PAS ANALYSE : \s:PSS_Kaw,c:1480370235*63\$KWVSI,ELM599,2,220003.337800,125,,*07'
!ABVDM,2,2,7,A,00000000000,2*28
aistool.tasks.header.0: INFO: Done.
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: DONE.
"""


def test_header_satais_alg(tasks, data):
    header._register(tasks)
    args = ['-v', 'header', 'satais_alg']

    with open(data) as f:
        inputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_header_satais_alg #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
clichain.cli: INFO: create: header
clichain.cli: INFO: creating pipeline...
aistool.tasks.header.0: INFO: starting: timestamp format: 
{'split_header': '[^!]+', 'capture_timestamp': '^([^!]+)', 'timestamp_format': '%d/%m/%y-%H:%M:%S', 'ignore': None}
clichain.cli: INFO: processing...
clichain.cli: INFO: ----------------------------------------
!ABVDM,1,1,,A,18J6uh05QpwCM=hHrGK12hn00<13,0*40
!ABVDM,1,1,,A,13E`ca001VwIcDNHn>sMgJon28S8,0*53
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23
!ABVDM,2,2,7,A,00000000000,2*28
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23
!ABVDM,2,2,7,A,00000000000,2*28
!ABVDM,2,2,7,A,00000000000,2*28
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# sat ais alg'
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72,1441789200
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# antilles guyane'
!KWVDM,1,1,2,B,1815De@02@LE8K42qtTd6aP406h0,0*09
aistool.tasks.header.0: ERROR: could not extract NMEA from line: 'PAS ANALYSE : \s:PSS_Kaw,c:1480370235*63\$KWVSI,ELM599,2,220003.337800,125,,*07'
!ABVDM,2,2,7,A,00000000000,2*28
aistool.tasks.header.0: INFO: Done.
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: DONE.
"""


def test_header_antilles_guyane(tasks, data):
    header._register(tasks)
    args = ['-v', 'header', 'antilles_guyane']

    with open(data) as f:
        inputs = f.read()
        
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_header_antilles_guyane #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
clichain.cli: INFO: create: header
clichain.cli: INFO: creating pipeline...
aistool.tasks.header.0: INFO: starting: timestamp format: 
{'split_header': '[^!]+', 'capture_timestamp': '^\\\\\\\\s:[^,]+,c:(\\\\d+)', 'timestamp_format': '%s', 'ignore': '^PAS ANALYSE'}
clichain.cli: INFO: processing...
clichain.cli: INFO: ----------------------------------------
!ABVDM,1,1,,A,18J6uh05QpwCM=hHrGK12hn00<13,0*40
!ABVDM,1,1,,A,13E`ca001VwIcDNHn>sMgJon28S8,0*53
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23
!ABVDM,2,2,7,A,00000000000,2*28
!ABVDM,2,1,7,A,54V5>V025k>l?ILCH01LE=B0hDt000000000001J1Qcw<6tD@UU1CQDRAQ@0,0*23
!ABVDM,2,2,7,A,00000000000,2*28
!ABVDM,2,2,7,A,00000000000,2*28
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# sat ais alg'
!AIVDM,1,1,,,1>j9e2wP000MlLdNfAk03Ov80000,0*72
aistool.tasks.header.0: ERROR: could not extract NMEA from line: '# antilles guyane'
!KWVDM,1,1,2,B,1815De@02@LE8K42qtTd6aP406h0,0*09,1480370235
!ABVDM,2,2,7,A,00000000000,2*28
aistool.tasks.header.0: INFO: Done.
clichain.cli: INFO: ----------------------------------------
clichain.cli: INFO: DONE.
"""
