#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import click
import ast
from clichain import pipeline
from aistool.app import config


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks


def test_implement_task(tasks):
    @pipeline.task
    def add_offset(ctrl, offset):
        print(f'{ctrl.name}: starting, offset = {offset}')
        with ctrl as push:
            while True:
                value = int((yield))
                push(value + offset)
        print(f'{ctrl.name}: offset task finished, no more value')

    @tasks
    @click.command(name='offset')
    @click.argument('offset')
    def offset_cli(offset):
        "add offset to value"
        offset = ast.literal_eval(offset)
        return add_offset(offset)

    inputs = '\n'.join('12345')
    args = ['offset', '10']
    result = app.test(args, inputs, catch_exceptions=False)
    print('>> test_implement_task #1:\n', result.output, file=sys.stderr)
    assert result.output == """\
0: starting, offset = 10
11
12
13
14
15
0: offset task finished, no more value
"""
