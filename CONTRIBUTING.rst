Contributing
==================

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 

.. todo:: pull requests process

Any modification will be made in a branch, no commit should be made in the "master" branch.

In a nutshell the process for making modifications is as follows:

+ create a 'x' branch from 'master'

+ commit in the 'x' branch

+ merge 'x' in 'master' without fast-forward to keep clear history

+ destroy 'x'
