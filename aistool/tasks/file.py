#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" Writes data into a text file, using a new line for each item

    .. seealso:: `usage`
"""

import logging
import click
from clichain import pipeline
from aistool.app import config, tasks
from functools import wraps


logger = logging.getLogger(__name__)


def usage():
    return """ Writes data into the specified file as text.

        The string representation of each processed item is written on
        a single line into the specified file.

        **NOTE**: sends input data (without alteration) to the next
        stage of the pipeline if any.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='file', help=usage())
    @click.argument('file', type=click.Path(exists=False))
    @wraps(cli)
    def _cli(file):
        return cli(file)

    return _cli


def cli(output):
    """ `click` command, see `usage` """
    return file_task(output)


_register(tasks)


@pipeline.task
def file_task(ctrl, output):
    """ Writes string representation of each input data into `output`.

        a new line is used for each input data, and input data is sent
        (without alteration) to the next stage of the pipeline if any.

        .. seealso:: `usage`
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    _logger.info(f'output file: {output}')

    with open(output, 'w') as f:
        with ctrl as push:
            while True:
                data = yield
                f.write(f'{data}\n')
                push(data)

    _logger.info('Done.')
