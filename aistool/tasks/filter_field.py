#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" filter decoded AIS messages based on a specific field.

    filter decoded AIS messages by comparing a specific field to one or
    several accepted values.

    .. seealso:: `usage`
"""

import logging
import click
import ast
from clichain import pipeline
from aistool.app import config, tasks
import sys
from functools import wraps


logger = logging.getLogger(__name__)


def usage():
    return """ filter messages based on a specific field.

        decoded messages are filtered by comparing a specific field to
        one or several accepted values.

        input messages are expected to be a dict containing all the
        decoded fields.

        **FIELD** specifies which field to compare in the message data.

        **VALUES** specifies the accepted values for the specified
        field. It can be either a single value or a sequence of values,
        example: ::

            \b
            # single value:
            '319018000'
            '"FAD2407"'
            'True'

            \b
            # the followings are equivalent:
            '227393830, 319018000, 992476133, 992479633'
            '[227393830, 319018000, 992476133, 992479633]'
            '(227393830, 319018000, 992476133, 992479633)'
            '(227393830,319018000,992476133,992479633)'

        **NOTE**: accepted value(s) must be basic types such as integer
        or strings, True/False are also accepted.
    """


usage.__doc__ = usage().replace('\b', '')


# usefull for tests
def _register(tasks):
    @tasks
    @click.command(name='filter_field', help=usage())
    @click.argument('field')
    @click.argument('values')
    @wraps(cli)
    def _cli(*args, **kw):
        return cli(*args, **kw)

    return _cli


def cli(field, values):
    """ `click` command, see `usage` """
    try:
        values = ast.literal_eval(values)
    except:
        raise click.BadParameter(f"incorrect values specification: {values}")

    return filter_field(field, values)


_register(tasks)


@pipeline.task
def filter_field(ctrl, field, values):
    """ filter messages based on `field` value.

        .. note:: input data is expected to be a `dict` containing
            all decoded AIS fields.

        if `field` value is contained in `values` the message
        is sent to the next stage, otherwise it's skipped.

        if `field` is not present in the message then the message is
        skipped.
    """
    _logger = logger.getChild(ctrl.name)
    _logger.info('starting')
    _logger.info(f'field: {field}')
    _logger.info(f'accepted values: {values}')

    try:
        values = set(values)
    except TypeError:
        values = set((values,))

    with ctrl as push:
        while True:
            msg = yield

            try:
                if msg[field] in values:
                    push(msg)
            except KeyError:
                continue

    _logger.info(f'Done.')
