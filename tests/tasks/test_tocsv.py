#!/usr/bin/env python

import pytest
import logging
import sys
import aistool.app as app
import aistool.tasks.tocsv as tocsv
from difflib import SequenceMatcher
import pathlib
import click
from clichain import pipeline 
import ast
import re
from datetime import datetime


@pytest.fixture
def data():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_default():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv_out_default.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_default_console():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv_out_default_console.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_datetimeformat():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv_out_datetimeformat.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_datetimeformat_console():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv_out_datetimeformat_console.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_csvoptions():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv_out_csvoptions.txt'
    assert data.exists()
    return data


@pytest.fixture
def output_csvoptions_console():
    path = pathlib.Path(__file__).parent
    data = path / 'data/debug_tocsv_out_csvoptions_console.txt'
    assert data.exists()
    return data


@pytest.fixture
def tasks():
    from clichain import cli
    tasks = cli.Tasks()
    _tasks = app.tasks
    app.tasks = tasks
    yield tasks
    app.tasks = _tasks

@pytest.fixture
def preprocess():
    def _preprocess(tasks):
        # 'date':
        # datetime.datetime(2016, 9, 30, 0, 0, 6) -> replace by (2016, 9, 30, 0, 0, 6)
        pattern =  "('date': )(datetime.datetime\\()([0-9, ]+)\\)"
        pattern = re.compile(pattern)

        @pipeline.task
        def pre_process(ctrl):
            parse = ast.literal_eval
            with ctrl as push:
                while True:
                    line = yield
                    line = pattern.sub("\\1(\\3)", line)  # 'date' field
                    data = parse(line)
                    try:
                        date = data['date']
                        data['date'] = datetime(*date) if date else None
                    except KeyError:
                        pass
                    push(data)

        @tasks
        @click.command(name='_parse')
        def _pre_process():
            return pre_process()

    return _preprocess


@pytest.mark.skip(reason="TODO: weird result sequence matching ratio")
def test_tocsv_help(tasks):
    tocsv._register(tasks)
    args = ['tocsv', '--help']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_tocsv_help #1:\n', result.output, file=sys.stderr)
    out = result.output.split('Options')
    ratio = SequenceMatcher(None, out[0], tocsv.usage()).ratio()
    assert ratio >= 0.8


def test_tocsv_invalid_fields_value(tasks):
    tocsv._register(tasks)
    args = ['tocsv', 'foo', 'bar']
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_tocsv_invalid_fields_value #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: bad 'fields' value: foo" in result.output


def test_tocsv_default(tasks, data, output_default, output_default_console,
                       tmpdir, preprocess):
    fields = '"x", "y", "mmsi", "nmea", "date"'
    tmp = tmpdir.mkdir("test_tocsv_default")
    tmpf = pathlib.Path(tmp) / 'test.csv'
    tocsv._register(tasks)
    preprocess(tasks)  # create _parse
    args = ['-v', '_parse', 'tocsv', fields, str(tmpf)]

    with open(output_default) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_default, mode='w') as f:
    #    with open(tmpf) as _f:
    #        f.write(_f.read())

    print('>> test_tocsv_default #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)

    with open(output_default_console) as f:
        assert result.output == f.read().replace('<<CSV_FILE_PATH>>', str(tmpf))
    with open(tmpf) as f:
        assert outputs == f.read()


def test_tocsv_datetime_format(tasks, data, output_datetimeformat,
                               output_datetimeformat_console,
                               tmpdir, caplog, preprocess):
    fields = '"x", "y", "mmsi", "nmea", "date"'
    tmp = tmpdir.mkdir("test_tocsv_datetime_format")
    tmpf = pathlib.Path(tmp) / 'test.csv'
    tocsv._register(tasks)
    preprocess(tasks)  # create _parse
    args = ['-v', '_parse', 'tocsv', '--datetimeformat', "%d/%m/%y-%H:%M:%S",
            fields, str(tmpf)]

    with open(output_datetimeformat) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_datetimeformat, mode='w') as f:
    #    with open(tmpf) as _f:
    #        f.write(_f.read())

    print('>> test_tocsv_datetime_format #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)

    with open(output_datetimeformat_console) as f:
        assert result.output == f.read().replace('<<CSV_FILE_PATH>>', str(tmpf))
    with open(tmpf) as f:
        assert outputs == f.read()


def test_tocsv_csv_options(tasks, data, output_csvoptions,
                           output_csvoptions_console,
                           tmpdir, preprocess):
    fields = '"x", "y", "mmsi", "nmea", "date"'
    tmp = tmpdir.mkdir("test_tocsv_csv_options")
    tmpf = pathlib.Path(tmp) / 'test.csv'
    tocsv._register(tasks)
    preprocess(tasks)  # create _parse
    args = ['-v', '_parse', 'tocsv',
            '--field_delimiter', ';',
            '--quote_char', "'",
            '--quote_policy', 'QUOTE_ALL',
            '--default_value', '999999999999999',
            fields, str(tmpf)]

    with open(output_csvoptions) as f:
        outputs = f.read()

    with open(data) as f:
        result = app.test(args, f, catch_exceptions=False)

    # create file
    #with open(output_csvoptions, mode='w') as f:
    #    with open(tmpf) as _f:
    #        f.write(_f.read())

    print('>> test_tocsv_csv_options #1:\n', file=sys.stderr)
    print(result.output, file=sys.stderr)

    with open(output_csvoptions_console) as f:
        assert result.output == f.read().replace('<<CSV_FILE_PATH>>', str(tmpf))
    with open(tmpf) as f:
        assert outputs == f.read()


def test_tocsv_invalid_quote_policy_option(tasks, tmpdir):
    tmp = tmpdir.mkdir("test_tocsv_invalid_quote_policy_option")
    tmpf = pathlib.Path(tmp) / 'test.csv'
    tocsv._register(tasks)
    args = ['tocsv', '--quote_policy', 'FOO', '"x", "y"', str(tmpf)]
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_tocsv_invalid_quote_policy_option #1:\n', result.output, file=sys.stderr)
    assert "Error: Invalid value: bad 'quote_policy': FOO" in result.output


def test_tocsv_invalid_csv_options(tasks, tmpdir):
    tmp = tmpdir.mkdir("test_tocsv_invalid_csv_options")
    tmpf = pathlib.Path(tmp) / 'test.csv'
    tocsv._register(tasks)
    args = ['tocsv', '--quote_char', '<<<<<', '"x", "y"', str(tmpf)]
    result = app.test(args, '', catch_exceptions=False)
    print('>> test_tocsv_invalid_csv_options #1:\n', result.output, file=sys.stderr)
    assert 'Error: Invalid value: "quotechar" must be a 1-character string' in result.output
